#!/bin/bash

set -eu

cd `dirname $0`/..

# .env file is required
if [[ -f .env ]]; then
    echo -e '\033[0;32mEnvironment variables will be exporting from .env file\033[0m'
else
    echo -e  '\033[0;31m.env file is not exists\033[0m';
    exit;
fi

# Copy php.ini if not exists
cp -u docker/dev/etc/php/php.ini.example docker/dev/etc/php/php.ini
cp -u docker/etc/php/php.ini.example docker/etc/php/php.ini

# Import .env variables
source <(grep -v '^#' .env | sed -E 's|^(.+)=(.*)$|: ${\1=\2}; export \1|g')

# Build required containers
docker build -f docker/containers/app/Dockerfile --build-arg=UID=$(id -u) --build-arg=GID=$(id -g) -t wex/app:latest .
docker build -f docker/containers/echo/Dockerfile --build-arg=UID=$(id -u) --build-arg=GID=$(id -g) -t wex/echo:latest .
docker build -f docker/containers/web/Dockerfile --build-arg=UID=$(id -u) --build-arg=GID=$(id -g) -t wex/web:latest .

# Build dev containers
docker build -f docker/dev/containers/web/Dockerfile --build-arg=UID=$(id -u) --build-arg=GID=$(id -g) -t wex/web:dev .
docker build -f docker/dev/containers/app/Dockerfile --build-arg=UID=$(id -u) --build-arg=GID=$(id -g) -t wex/app:dev .

echo 'Starting development containers'
CURRENT_USER=$(id -u):$(id -g) docker-compose -f docker-compose.yml -f docker-compose.local.yml -p wex up -d
