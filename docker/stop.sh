#!/bin/bash

cd `dirname $0`/..

docker container stop $(docker ps -f name=wex -q)

echo 'Containers has been successfully stopped'
