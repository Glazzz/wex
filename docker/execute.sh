#!/bin/bash

CONTAINER=$1
COMMAND=$2

if [[ "$CONTAINER" = "" ]]; then
    CONTAINER=$(docker ps | grep wex_app | awk '{print $1}')
fi

if [[ "$COMMAND" = "" ]]; then
    docker exec -it ${CONTAINER} /bin/sh
else
    docker exec -it ${CONTAINER} ${COMMAND} $3 $4
fi
