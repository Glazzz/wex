#!/bin/sh

set -e

rm -rf /app/bootstrap/cache/*.php || true
rm -rf /app/storage/cache/* || true

mkdir -p -m 0777 /app/bootstrap/cache
mkdir -p -m 0777 /app/storage/framework/views
mkdir -p -m 0777 /app/storage/framework/cache
mkdir -p -m 0777 /app/storage/framework/sessions
mkdir -p -m 0777 /app/storage/logs
mkdir -p -m 0777 /app/tests/coverage

# need to clear config if we want to use env() function
php artisan config:clear

exec "$@"
