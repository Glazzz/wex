## WEX Application

### Install application

To automatically install the project, you can run the `./init.sh` command in the console.

--------------------

If you want to install manually:
1. copy the contents of the .env.template file into the .env file and edit the necessary values.

    Pay attention to the following values:
    - APP_ENV - name of the environment in which the project is launched, may be local, development, testing or production
    - APP_DEBUG - whether to enable debug-bar
    - APP_PROTOCOL - http or https (use http for local env)
     
    - DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, DB_PASSWORD - credentials for database connection.
    
    - REDIS_HOST, REDIS_PASSWORD, REDIS_PORT - credentials for redis connection .
    
    - DOCKER_WEB_HOST - set the desired value for the local project domain.
    - DOCKER_ECHO_PORT - port number for the service web-socket
    - DOCKER_REDIS_PORT - port number for redis
    - DOCKER_WEB_PORT - on which port to open nginx connection on the host
    - DOCKER_DB_PORT - to connect to the database from the host.
    
    - STATIC_ANALYSIS_IGNORE_FIXER - ignore static analysis code before each commit
    - STATIC_ANALYSIS_IGNORE_ERRORS - ignore static analyzer error

2. Build and run docker containers by command `./docker/start.sh`

    Ensure that you have all continers by command `docker ps -f name=wex`
    
    For stopping containers use command `./docker/stop.sh`
    
    If you want to stop, delete all containers and docker images then use command `./docker/docker-clear.sh`
    
    If you want to execute some command inside docker container use command `./docker/execute.sh` for example `./docker/execute.sh wex_app php artisan migrate`
    
3. generate application key (only for first application run): `./docker/execute.sh wex_app php artisan key:generate`

4. Execute migrations `./docker/execute.sh wex_app php artisan migrate`

5. Execute seeds (only for first application run): `./docker/execute.sh wex_app php artisan db:seed`
