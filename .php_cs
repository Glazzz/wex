<?php

$finder = PhpCsFixer\Finder::create()
    ->notPath('bootstrap/cache')
    ->notPath('storage')
    ->notPath('vendor')
    ->notPath('docker')
    ->notPath('node_modules')
    ->notPath('public')
    ->in(__DIR__)
    ->name('*.php')
    ->notName('*.blade.php')
    ->ignoreDotFiles(true)
    ->ignoreVCS(true);

return PhpCsFixer\Config::create()
    ->setRiskyAllowed(true)
    ->setRules([
        '@PSR2' => true,
        '@Symfony' => true,
        '@Symfony:risky' => true,
        '@PHP71Migration:risky' => true,
        '@PHP73Migration' => true,
        'declare_strict_types' => false,
        'dir_constant' => true,
        'array_syntax' => ['syntax' => 'short'],
        'no_useless_return' => true,
        'linebreak_after_opening_tag' => true,
        'phpdoc_order' => true,
        'is_null' => true,
        'phpdoc_add_missing_param_annotation' => [
            'only_untyped' => false,
        ],
        'no_empty_phpdoc' => true,
        'phpdoc_align' => [
            'align' => 'left',
        ],
        'no_useless_else' => true,
        'ordered_imports' => [
            'sortAlgorithm' => 'alpha',
        ],
        'no_php4_constructor' => true,
        'php_unit_construct' => true,
        'php_unit_strict' => true,
        'align_multiline_comment' => true,
        'backtick_to_shell_exec' => true,
        'combine_consecutive_unsets' => true,
        'fully_qualified_strict_types' => true,
        'hash_to_slash_comment' => true,
        'method_chaining_indentation' => true,
        'mb_str_functions' => false, // dangerous
        'no_null_property_initialization' => true,
        'no_mixed_echo_print' => true,
        'no_multiline_whitespace_before_semicolons' => true,
        'no_whitespace_before_comma_in_array' => true,
        'phpdoc_indent' => true,
    ])
    ->setFinder($finder)
;
