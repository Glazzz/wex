#!/bin/bash

HOST=$1

if [[ "$HOST" = "" ]]; then
    echo 'Please type a host name'
    exit;
fi

if [[ -n "$(grep $HOST /etc/hosts)" ]]; then
    echo -e "\033[0;31mHost '${HOST}' already exists\033[0m"
else
    sudo echo 127.0.0.1 $HOST >> /etc/hosts
    echo -e "\033[0;32m${HOST} has been successfully added\033[0m"
fi
