#!/bin/bash

set -eu

sudo -k

# Install composer
if ! which composer &> /dev/null; then
    curl -sS https://getcomposer.org/installer | php
    sudo mv composer.phar /usr/bin/composer

    printf "Composer has been installed successfully\n\n"
    composer --version
else
    printf "Composer is installed\n"
fi

# Install Docker
if ! which docker &> /dev/null; then
sudo apt update \
    && sudo apt-get install software-properties-common --yes \
    && sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic test" \
    && sudo apt --yes --no-install-recommends install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common \
    && wget --quiet --output-document=- https://download.docker.com/linux/ubuntu/gpg \
        | sudo apt-key add - \
    && sudo add-apt-repository \
        "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/ubuntu \
        $(lsb_release --codename --short) \
        stable" \
    && sudo apt update \
    && sudo apt --yes --no-install-recommends install docker-ce \
    && sudo usermod --append --groups docker "$USER" \
    && sudo systemctl enable docker \
    && printf '\nDocker installed successfully\n\n'

    printf 'Waiting for Docker to start...\n\n'
    sleep 3
else
    printf "Docker is installed\n"
fi

# Install Docker Compose
if ! which docker-compose &> /dev/null; then
    sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose

    # Optional symlink
    sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

    printf "Docker Compose has been installed successfully\n\n"
else
    printf "Docker Compose is installed\n"
fi

# Install PHPUnit
if ! which phpunit &> /dev/null; then
    composer global require --dev phpunit/phpunit ^7.0
    sudo ln -s ~/.config/composer/vendor/bin/phpunit /usr/local/bin/phpunit

    printf "Composer has been installed successfully\n\n"

    phpunit --version
else
    printf "PHPUnit is installed\n"
fi
