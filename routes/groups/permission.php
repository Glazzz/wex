<?php
// Please don't use require_once instead require. It creates bug with route caching and phpunit
require 'permission/core.php';
require 'permission/user.php';
require 'permission/reports.php';
