<?php

return [
    'private_key' => secret_env('COINPAYMENT_PRIVATE_KEY', ''),
    'public_key' => secret_env('COINPAYMENT_PUBLIC_KEY', ''),
    'merchant_id' => secret_env('COINPAYMENT_MERCHANT_ID', ''),
    'ipn_secret' => secret_env('COINPAYMENT_IPN_SECRET', ''),
    'ipn_url' => secret_env('COINPAYMENT_IPN_URL', ''),
    'ch' => secret_env('COINPAYMENT_CH', null),
];
