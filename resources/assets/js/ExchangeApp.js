import CoinMarket from './components/exchange/CoinMarket';

var ExchangeApp = new Vue({
  el: '#app',
  data: {
    coinMarket: CoinMarket
  }
});
