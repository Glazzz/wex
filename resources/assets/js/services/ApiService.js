import axios from 'axios'

class ApiService {
  constructor () {
    this.axios = axios.create()
  }

  post (path, body = null, query = {}, headers = {}) {
    return this.axios.post(path, body, {
      params: query,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        ...headers
      }
    })
  }

  postForm (path, formData, query = {}, headers = {}) {
    return this.axios.post(path, formData, {
      params: query,
      headers: {
        ...headers,
        'Content-Type': 'multipart/form-data'
      }
    })
  }

  get (path, query) {
    return this.axios.get(path, {
      params: query,
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
  }
}

export default new ApiService()
