@extends('backend.layouts.top_navigation_layout')
@section('title', 'Home')
@section('after-style')
    <link rel="stylesheet" href="{{asset('assets/frontend/style.css')}}">
@endsection
@section('content')
    <div class="fullwidth">
        <div class="parallax-window header-main-section" data-parallax="scroll"
             style="background-image:url(/assets/frontend/images/banner.jpg); background-attachment: fixed; background-size: cover;">
            <div class="overlay-dark">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-sm-12 order-sm-first">
                            <div class="banner-text">
                                <h1>WhiteEX - Community Exchange</h1>
                                <p>Welcome to one of the most friendly and fast growing crypto exchanges in the world</p>
                                <div class="banner-text-buttons">
                                    <a href="/register"><div class="green-button">Open a live account</div></a>
                                    <a href="/login"><div class="white-button">Sign in</div></a>
                                    <div style="background-image:url(/assets/frontend/images/banner.jpg); background-attachment: fixed; background-size: cover;">
                                        <div style="padding: 60px;">
                                            <div class="marquee-container">
                                                <div class="marquee-inner-container">
                                                    <div class="marquee" data-time="{{ 11400+1800*$stockPairs->count() }}">
                                                        @foreach($stockPairs as $stockPair)
                                                            <div id="stock_pair{{ $stockPair->id }}" class="marquee-scroll-box">
                                                                <h4>{{ $stockPair->stock_item_abbr }}/{{ $stockPair->base_item_abbr }}</h4>
                                                                <span class="middark-color font-12">
                                                                    {{ __('Last Price') }}
                                                                    <span class="text-white last_price">{{ number_format($stockPair->last_price) }}</span>
                                                                </span>
                                                                <br>
                                                                <span class="font-12">
                                                                    @if($stockPair->change_24 > 0)
                                                                        <i class="fa fa-sort-up text-green"></i>
                                                                    @elseif($stockPair->change_24 < 0)
                                                                        <i class="fa fa-sort-down text-red"></i>
                                                                    @else
                                                                        <i class="fa fa-sort text-gray"></i>
                                                                    @endif
&nbsp;                                                                   <span class="text-white change_24">{{ number_format($stockPair->change_24,2) }}%</span>
                                                                </span>
                                                                <br>
                                                                <span class="middark-color font-12">
                                                                    {{ __('Volume') }}
                                                                    <span class="text-white volume">{{ number_format($stockPair->exchanged_base_item_volume_24, 3) }}</span>
                                                                </span>
                                                                <br>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container main-section-1">
            <img class="big-under-banner" src="/assets/images/trading-mockup.png" alt="">
            <div class="main-section-1-1 container">
                <h2>The most trusted digital currency platform</h2>
                <p>Organize your workspace according to your needs: compose your layout, choose between themes, set up notifications and data preferences.</p>
                <div class="col-md-12 main-section-1-1-advantages">
                    <div class="col-md-4 col-sm-4">
                        <div class="adventages-left-content">
                            <img src="/assets/images/advantages1.png" alt="">
                        </div>
                        <div class="adventages-right-content">
                            <span class="advantages-title">information about</span>
                            <h4>Secure storage</h4>
                            <p>Multi-level security, the exchange team does not store your keys</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="adventages-left-content">
                            <img src="/assets/images/advantages2.png" alt="">
                        </div>
                        <div class="adventages-right-content">
                            <span class="advantages-title">information about</span>
                            <h4>Growing functionality</h4>
                            <p>Constant updates, and step-by-step development of the DEX architecture</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="adventages-left-content">
                            <img src="/assets/images/advantages3.png">
                        </div>
                        <div class="adventages-right-content">
                            <span class="advantages-title">information about</span>
                            <h4>Customizable Interface</h4>
                            <p>Ability to change the interface according to your desire (in development)</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="adventages-left-content">
                            <img src="/assets/images/advantages4.png">
                        </div>
                        <div class="adventages-right-content">
                            <span class="advantages-title">information about</span>
                            <h4>Low commission rates</h4>
                            <p>Commissions are minimal, but at the same time all BTW Holders who participate in the “Frost” program had a steady income</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="adventages-left-content">
                            <img src="/assets/images/advantages5.png">
                        </div>
                        <div class="adventages-right-content">
                            <span class="advantages-title">information about</span>
                            <h4>Full transparency in front of the community</h4>
                            <p>Everything that we do and everything that we have in mind, you can always observe</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="adventages-left-content">
                            <img src="/assets/images/advantages6.png">
                        </div>
                        <div class="adventages-right-content">
                            <span class="advantages-title">information about</span>
                            <h4>Secure & 24/7 monitoring</h4>
                            <p>Secured platfrom & 24/7/365 monitoring system</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-section-1-2 container">
                <div class="pull-left">
                    <h2>Advanced trading tools</h2>
                    <p>Various trading tools, different types of charts, oscillators (and other tools that are not very effective individually) are available for use! But if you are new to trading, you better learn on a clean chart, and remember that cryptocurrencies are high-risk assets. Do not take loans in banks for trading (Unless you own insider information - In this case, you can send the inside information to the team and community too)</br></br>We also boast a suite of order types to help traders take advantage of every situation.</p>
                </div>
                <div class="pull-right">
                    <img src="/assets/images/section-1-2-image.png">
                </div>
            </div>
            <div class="main-section-1-3 container">
                <div class="pull-left">
                    <img src="/assets/images/section-1-3-mobile-app.png">
                </div>
                <div class="pull-right">
                    <h2>We've created the mobile tools you need</h2>
                    <p>WhiteEX - operate the premier U.S.-based blockchain trading platform, which is designed for customers who demand lightning-fast trade execution, dependable digital wallets, and industry-leading security practices.</p>
                    <a href="#" class="advantages-read-more">APP Download →</a>
                    <div class="col-md-12 col-sm-12">
                        <div class="adventages-left-content">
                            <img src="/assets/images/advantages7.png">
                        </div>
                        <div class="adventages-right-content">
                            <h4>Customizable interface</h4>
                            <p>We've created the mobile tools you need for trading on the go.</p>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="adventages-left-content">
                            <img src="/assets/images/advantages8.png">
                        </div>
                        <div class="adventages-right-content">
                            <h4>Margin trading</h4>
                            <p>Organize your workspace according to your needs compose your layout, choose between themes, set up notifications and data preferences.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="parallax-window section-space main-section-2" data-parallax="scroll" data-image-src="/assets/images/main-section-2-bg.jpg"
             style="background-image:url(/assets/images/main-section-2-bg.jpg); background-attachment: fixed; background-size: cover; ">
            <div class="container">
                <div class="col-md-4">
                    <div class="section-2-image-wrap"><img src="/assets/images/advantages9.png"></div>
                    <p>24-hour support</p>
                </div>
                <div class="col-md-4">
                    <div class="section-2-image-wrap"><img src="/assets/images/advantages10.png"></div>
                    <p>Growing functionality</p>
                </div>
                <div class="col-md-4">
                    <div class="section-2-image-wrap"><img src="/assets/images/advantages11.png"></div>
                    <p>Unlimited deposits & withdrawals</p>

                </div>
            </div>
        </div>

        <div class="container main-section-3">
            <div class="container">
                <h2>Customer stories</h2>
                <p>We operate the premier U.S.-based blockchain trading platform, which is designed for customers who demand lightning-fast trade execution, dependable digital wallets, and industry-leading security practices.</p>
                <a href="#" class="advantages-read-more">See more stories →</a>

            </div>
        </div>

        <footer class="footer">
            <div class="main-section-4">
                <h2>
                    Welcome to one of the most friendly and fast growing crypto exchanges in the world
                </h2>
                <a href="/register"><div class="green-button">Set up trading account</div></a>
                <p>Already a member? <a href="/login">Sign in.</a></p>
            </div>
            <div class="top-footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="container">
                                <div class="footer-logo">
                                    <img src="/assets/images/footer-logo.png">
                                </div>
                                <div class="footer-company">
                                    <div class="footer-title">Link</div>
                                    <a href="#">Become a Vendor</a></br>
                                    <a href="#">Affiliates</a></br>
                                    <a href="https://whiteex.net/faq/22">Terms of Use</a></br>
                                    <a href="https://whiteex.net/faq/21">Privacy Policy</a></br>
                                </div>
                                <div class="footer-contact">
                                    <div class="footer-title">Contact</div>
                                    <p>Support： support@whiteex.net</p>
                                    <p>Listing Application： listing@whiteex.net</p>
                                    <p>Twitter： @whiteexnet</p>
                                    <a href=""><i class="fab fa-slack-hash"></i></a>
                                    <a href=""><i class="fab fa-skype"></i></a>
                                    <a href="https://t.me/whiteexnet"><i class="fab fa-telegram"></i></i></a>
                                    <a href=""><i class="fab fa-reddit"></i></a>
                                    <a href=""><i class="fab fa-github"></i></a>
                                </div>
                                <div class="footer-apps">
                                    <div class="footer-title">App Download</div>
                                    <a href="#"><div class="footer-app-store-app"><i class="fab fa-apple"></i>App Store</div></a>
                                    <a href="#"><div class="footer-play-market-app"><i class="fab fa-google-play"></i></i>Play Store</div></a>
                                </div>
                            </div>

                        <!-- Social

                        <ul class="floated-li-inside clearfix centered">
                            {!! social_media_link('facebook') !!}
                        {!! social_media_link('twitter') !!}
                        {!! social_media_link('linkedin') !!}
                        {!! social_media_link('google_plus') !!}
                        {!! social_media_link('pinterest') !!}
                                </ul>-->
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </footer>
    </div>
@endsection

@section('script')
    <script>
      (function ($) {
        $(document).on('click', '.dropdown-item', function () {
          $(this).parent().siblings('.dropdown-toggle').find('.text-dropdown').text($(this).data('val'));
        });

        $('.marquee').each(function () {
          var $this = $(this);
          var parentWidth = $this.parent().outerWidth();
          var scrollTime = +$this.data('time');
          var childWidth = $this.children().eq(0).outerWidth();
          var sublength = $this.children().length;
          var thisWidth = childWidth * (sublength - 1);
          if (parentWidth < thisWidth) {
            $this.css({
              'margin-left': (parentWidth - thisWidth) + 'px'
            });
          } else {
            thisWidth = parentWidth;
          }
          $this.width(thisWidth);
          var unitDelay = Math.round(scrollTime * childWidth / (thisWidth + childWidth));
          for (var i = 0; i < sublength; i++) {
            var thisItemDelay = unitDelay * i;
            // if(thisItemDelay != 0){
            thisItemDelay = thisItemDelay + 'ms';
            // }
            $this.children().eq(i).css({
              'animation': 'scroll-now ' + scrollTime + 'ms linear ' + thisItemDelay + ' infinite',
              '-moz-animation': 'scroll-now ' + scrollTime + 'ms linear ' + thisItemDelay + ' infinite',
              '-webkit-animation': 'scroll-now ' + scrollTime + 'ms linear ' + thisItemDelay + ' infinite',
              '-o-animation': 'scroll-now ' + scrollTime + 'ms linear ' + thisItemDelay + ' infinite',
              '-ms-animation': 'scroll-now ' + scrollTime + 'ms linear ' + thisItemDelay + ' infinite'
            });
          }
        });

        Echo.channel('exchange').listen('Exchange.BroadcastStockSummary', (data) => {
          updateMarque(data.stockSummary);
        });

      })(jQuery);

      function updateMarque(data) {
        let marque = $('#stock_pair' + data.stock_pair_id);
        marque.find('.last_price').text(number_format(data.last_price));
        marque.find('.change_24').text(number_format(data.change_24));
        marque.find('.volume').text(number_format(data.exchanged_base_item_volume_24));
      }
    </script>
@endsection
