@extends('backend.layouts.top_navigation_layout')
@section('title', $title)
@section('after-style')
    <link href="{{ asset('/assets/css/exchange.css')  }}" rel="stylesheet">
@endsection
@section('content')
    <div class="row">
        <div class="col-md-4">
            @include('frontend.exchange.stock_market')
        </div>
        <div class="col-md-6">
            <div class="box box-borderless full-in-small">
                <div class="box-header" style="padding-bottom: 5px;border-bottom:1px solid #efefef">
                    @include('frontend.exchange.stock_pair_summary')
                </div>

                <div class="box-body" style="padding-top: 10px">
                    @include('frontend.exchange.chart')
                </div>
            </div>
        </div>
        <div class="col-md-2">

        </div>
    </div>



    <div class="row">
        <div class="col-md-4">
            @include('frontend.exchange.buy_form')
        </div>

        <div class="col-md-4">
            @include('frontend.exchange.stop_limit_form')
        </div>

        <div class="col-md-4">
            @include('frontend.exchange.sell_form')
        </div>
    </div>

    @include('frontend.exchange.order_book')
    @auth
        @include('frontend.exchange.my_order')
    @endauth

    @include('frontend.exchange.trade_history')

@endsection

@section('script')
    <script src="{{asset('assets/common/vendors/bcmath/libbcmath-min.js')}}"></script>
    <script src="{{asset('assets/common/vendors/bcmath/bcmath.js')}}"></script>
    <script src="{{asset('assets/common/vendors/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script src="{{asset('assets/common/vendors/datatable_responsive/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('assets/common/vendors/datatable_responsive/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"></script>
    <script src="{{asset('assets/common/vendors/echart/echarts.min.js')}}"></script>
    <script src="{{asset('/assets/js/chart.js')}}"></script>
    <script src="{{asset('assets/common/vendors/cvalidator/cvalidator.js')}}"></script>

    @include('frontend.exchange.initial_js')

    @include('frontend.exchange.broadcast_js')

    @include('frontend.exchange.custom_function_js')

@endsection
