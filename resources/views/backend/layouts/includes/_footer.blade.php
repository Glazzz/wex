<footer class="main-footer container">
   <div class="pull-left"><p>© Copyright WhiteEX 2019 | All rights reserved.</p></div>
   <div class="pull-right">
   	<a href="#">Terms & Conditions</a>
   	<a href="#">For Partners</a>
   	<a href="#">FAQ</a>
   	<a href="#">Contact Us</a>
   </div>
</footer>

<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
@include('errors.flash_message')
<!-- jQuery 3 -->
<script src="{{ asset('assets/js/app.js') }}"></script>
<script src="{{ asset('assets/common/vendors/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/common/vendors/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/common/vendors/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/backend/assets/js/adminlte.min.js') }}"></script>
@yield('extraScript')
<script src="{{ asset('assets/backend/assets/js/custom.js') }}"></script>
@yield('script')
</body>
</html>
