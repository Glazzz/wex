<?php

namespace Tests;

use App\Http\Middleware\EncryptCookies;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Mockery;
use Mockery\MockInterface;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * @param string $class
     *
     * @return MockInterface
     */
    public function mock(string $class): MockInterface
    {
        $mock = Mockery::mock($class);

        $this->app->instance($class, $mock);

        return $mock;
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

    /**
     * @param string $cookies
     *
     * @return $this
     */
    protected function disableCookiesEncryption(string $cookies): self
    {
        $this->app->resolving(EncryptCookies::class, function($object) use ($cookies) {
            $object->disableFor($cookies);
        });

        return $this;
    }
}
