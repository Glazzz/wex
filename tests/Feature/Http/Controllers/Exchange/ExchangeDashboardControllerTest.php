<?php

namespace Tests\Feature\Http\Controllers\Exchange;

use App\Models\Backend\StockPair;
use App\Services\Backend\ExchangeDashboardService;
use Mockery\MockInterface;
use Tests\TestCase;

/**
 * @coversDefaultClass \App\Http\Controllers\Exchange\ExchangeDashboardController
 */
class ExchangeDashboardControllerTest extends TestCase
{
    /** @var ExchangeDashboardService|MockInterface */
    private $exchangeService;

    public function setUp(): void
    {
        parent::setUp();

        $this->exchangeService = $this->mock(ExchangeDashboardService::class);
    }

    /**
     * @covers ::__construct
     * @covers ::index
     */
    public function testIndex(): void
    {
        $stockPair = new StockPair();

        $this->exchangeService
            ->shouldReceive('getDefaultStockPair')
            ->with(null)
            ->once()
            ->andReturn($stockPair);

        $response = $this->call('GET', 'exchange');

        $response->assertStatus(200);
        $response->assertViewIs('frontend.exchange.index');
        $response->assertViewHas('title', __('Exchange'));
        $response->assertViewHas('stockPair', $stockPair);
        $response->assertViewHas('categoryID', CATEGORY_EXCHANGE);
        $response->assertViewHas('chartInterval');
        $response->assertViewHas('chartZoom');
    }

    /**
     * @covers ::__construct
     * @covers ::index
     */
    public function testIndex_WithPair(): void
    {
        $pair = 'doge-btc';

        $stockPair = new StockPair();

        $this->exchangeService
            ->shouldReceive('getDefaultStockPair')
            ->with($pair)
            ->once()
            ->andReturn($stockPair);

        $response = $this->call('GET', \sprintf('exchange/%s', $pair));

        $response->assertStatus(200);
        $response->assertViewIs('frontend.exchange.index');
        $response->assertViewHas('title', __('Exchange'));
        $response->assertViewHas('stockPair', $stockPair);
        $response->assertViewHas('categoryID', CATEGORY_EXCHANGE);
        $response->assertViewHas('chartInterval');
        $response->assertViewHas('chartZoom');
    }

    /**
     * @covers ::__construct
     * @covers ::index
     */
    public function testIndex_NotValidPair(): void
    {
        $notValidPair = 'TEST-TEST';

        $this->exchangeService
            ->shouldReceive('getDefaultStockPair')
            ->with($notValidPair)
            ->once()
            ->andReturn(null);

        $response = $this->call('GET', \sprintf('exchange/%s', $notValidPair));

        $response->assertStatus(404);
    }
}
