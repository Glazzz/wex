<?php

namespace Tests\Unit\Services\Exchange;

use App\Models\Backend\StockPair;
use App\Repositories\User\Admin\Eloquent\StockPairRepository;
use App\Services\Backend\ExchangeDashboardService;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;
use Mockery\MockInterface;
use Tests\TestCase;

class ExchangeDashboardServiceTest extends TestCase
{
    /** @var StockPairRepository|MockInterface */
    private $stockPairRepository;

    /** @var Request|MockInterface */
    private $request;

    /** @var CookieJar|MockInterface */
    private $cookie;

    /** @var ExchangeDashboardService */
    private $exchangeDashboardService;

    public function setUp(): void
    {
        parent::setUp();

        $this->stockPairRepository = $this->mock(StockPairRepository::class);
        $this->request = $this->mock(Request::class);
        $this->cookie = $this->mock(CookieJar::class);

        $this->exchangeDashboardService = new ExchangeDashboardService(
            $this->stockPairRepository,
            $this->request,
            $this->cookie
        );
    }

    public function testGetDefaultStockPair_WithPair(): void
    {
        $stockItem = 'doge';
        $baseItem = 'btc';

        $pair = \sprintf('%s-%s', $stockItem, $baseItem);

        $stockPair = new StockPair();
        $stockPair->id = 1;

        $this->stockPairRepository
            ->shouldReceive('getByPair')
            ->with($stockItem, $baseItem)
            ->once()
            ->andReturn($stockPair);

        $this->cookie
            ->shouldReceive('forever')
            ->with(ExchangeDashboardService::COOKIE_STOCK_PAIR_ID, $stockPair->id)
            ->once();

        $this->assertEquals($stockPair, $this->exchangeDashboardService->getDefaultStockPair($pair));
    }

    public function testGetDefaultStockPair_WithoutPair(): void
    {
        $this->request
            ->shouldReceive('cookie')
            ->with(ExchangeDashboardService::COOKIE_STOCK_PAIR_ID)
            ->once()
            ->andReturn(null);

        $stockPair = new StockPair();
        $stockPair->id = 1;

        $this->stockPairRepository
            ->shouldReceive('getFirstByConditions')
            ->with([
                'is_active' => ACTIVE_STATUS_ACTIVE,
                'is_default' => ACTIVE_STATUS_ACTIVE,
            ])
            ->once()
            ->andReturn($stockPair);

        $this->cookie
            ->shouldReceive('forever')
            ->with(ExchangeDashboardService::COOKIE_STOCK_PAIR_ID, $stockPair->id)
            ->once();

        $this->assertEquals($stockPair, $this->exchangeDashboardService->getDefaultStockPair(null));
    }

    public function testGetDefaultStockPair_PairFromCookie(): void
    {
        $stockPairId = 1;

        $this->request
            ->shouldReceive('cookie')
            ->with(ExchangeDashboardService::COOKIE_STOCK_PAIR_ID)
            ->once()
            ->andReturn($stockPairId);

        $stockPair = new StockPair();
        $stockPair->id = $stockPairId;

        $this->stockPairRepository
            ->shouldReceive('getFirstById')
            ->with($stockPairId)
            ->once()
            ->andReturn($stockPair);

        $this->assertEquals($stockPair, $this->exchangeDashboardService->getDefaultStockPair(null));
    }
}
