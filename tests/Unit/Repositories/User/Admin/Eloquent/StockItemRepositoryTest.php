<?php

namespace Tests\Repositories\User\Admin\Eloquent;

use App\Models\Backend\StockItem;
use App\Repositories\User\Admin\Eloquent\StockItemRepository;
use Illuminate\Support\Collection;
use Mockery\MockInterface;
use Tests\TestCase;

/**
 * @coversDefaultClass \App\Repositories\User\Admin\Eloquent\StockItemRepository
 */
class StockItemRepositoryTest extends TestCase
{
    /** @var StockItem|MockInterface */
    private $stockItemModel;

    /** @var StockItemRepository */
    private $stockItemRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->stockItemModel = $this->mock(StockItem::class);

        $this->stockItemRepository = new StockItemRepository($this->stockItemModel);
    }

    /**
     * @covers ::getActiveList
     * @covers ::__construct
     */
    public function testGetActiveList(): void
    {
        $this->stockItemModel
            ->shouldReceive('select')
            ->once()
            ->andReturn($this->stockItemModel);

        $conditions = [
            'is_active' => ACTIVE_STATUS_ACTIVE,
            'item_type' => CURRENCY_CRYPTO,
        ];

        $this->stockItemModel
            ->shouldReceive('where')
            ->with($conditions)
            ->once()
            ->andReturn($this->stockItemModel);

        $collection = new Collection();

        $this->stockItemModel
            ->shouldReceive('get')
            ->once()
            ->withNoArgs()
            ->andReturn($collection);

        $stockItemType = CURRENCY_CRYPTO;

        $this->assertSame($collection, $this->stockItemRepository->getActiveList($stockItemType));
    }

    /**
     * @covers ::getCountByConditions
     * @covers ::__construct
     */
    public function testGetCountByConditions(): void
    {
        $conditions = ['is_active' => ACTIVE_STATUS_ACTIVE];

        $this->stockItemModel
            ->shouldReceive('where')
            ->with($conditions)
            ->once()
            ->andReturn($this->stockItemModel);

        $this->stockItemModel
            ->shouldReceive('count')
            ->withNoArgs()
            ->once()
            ->andReturn(10);

        $this->assertIsInt($this->stockItemRepository->getCountByConditions($conditions));
    }

    /**
     * @covers ::getStockPairsById
     */
    public function testGetStockPairsById(): void
    {
        $id = 1;

        $this->stockItemModel
            ->shouldReceive('where')
            ->with('id', $id)
            ->once()
            ->andReturn($this->stockItemModel);

        $this->stockItemModel
            ->shouldReceive('leftJoin')
            ->with('stock_pairs as base', 'base.base_item_id', '=', 'stock_items.id')
            ->once()
            ->andReturn($this->stockItemModel);

        $this->stockItemModel
            ->shouldReceive('leftJoin')
            ->with('stock_pairs as stock', 'stock.stock_item_id', '=', 'stock_items.id')
            ->once()
            ->andReturn($this->stockItemModel);

        $this->stockItemModel
            ->shouldReceive('select')
            ->once()
            ->with([
                'stock_items.*',
                'stock_item.id as stock_item_id',
                'stock_item.item as stock_item_abbr',
                'stock_item.item_name as stock_item_name',
                'stock_item.item_type as stock_item_type',
                'base_item.id as base_item_id',
                'base_item.item as base_item_abbr',
                'base_item.item_name as base_item_name',
                'base_item.item_type as base_item_type',
                'last_price',
                'exchange_24',
                'stock_pairs.base_item_buy_order_volume',
                'stock_pairs.stock_item_buy_order_volume',
                'stock_pairs.base_item_sale_order_volume',
                'stock_pairs.stock_item_sale_order_volume',
                'stock_pairs.exchanged_buy_total',
                'stock_pairs.exchanged_sale_total',
                'stock_pairs.exchanged_amount',
                'stock_pairs.exchanged_maker_total',
                'stock_pairs.exchanged_buy_fee',
                'stock_pairs.exchanged_sale_fee',
            ])
            ->andReturn($this->stockItemModel);

        $this->assertSame($this->stockItemModel, $this->stockItemRepository->getStockPairsById($id));
    }
}
