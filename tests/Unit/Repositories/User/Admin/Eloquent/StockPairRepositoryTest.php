<?php

namespace Tests\Unit\Repositories\User\Admin\Eloquent;

use App\Models\Backend\StockPair;
use App\Repositories\User\Admin\Eloquent\StockPairRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery\MockInterface;
use Tests\TestCase;

/**
 * @coversDefaultClass \App\Repositories\User\Admin\Eloquent\StockPairRepository
 */
class StockPairRepositoryTest extends TestCase
{
    use withFaker;

    /** @var StockPair|MockInterface */
    private $stockPairModel;

    /** @var StockPairRepository */
    private $stockPairRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->stockPairModel = $this->mock(StockPair::class);

        $this->stockPairRepository = new StockPairRepository($this->stockPairModel);
    }

    public function provideRowsForUpdate(): array
    {
        return [
            [
                [
                    'exchanged_buy_fee' => 0.02,
                    'exchanged_sell_fee' => 0.02,
                ],
            ],
        ];
    }

    /**
     * @covers ::updateRows
     * @covers ::__construct
     * @dataProvider provideRowsForUpdate
     *
     * @param array $attributes
     */
    public function testUpdateRowsWithCondition(array $attributes): void
    {
        $condition = ['is_active' => ACTIVE_STATUS_ACTIVE];

        $this->stockPairModel->shouldReceive('where')->with($condition)->once()->andReturn($this->stockPairModel);

        $this->stockPairModel->shouldReceive('update')->with($attributes)->once()->andReturnTrue();

        $this->assertTrue($this->stockPairRepository->updateRows($attributes, $condition));
    }

    /**
     * @covers ::updateRows
     * @covers ::__construct
     * @dataProvider provideRowsForUpdate
     *
     * @param array $attributes
     */
    public function testUpdateRowsWithoutCondition(array $attributes): void
    {
        $this->stockPairModel->shouldReceive('update')->with($attributes)->once()->andReturnTrue();

        $this->assertTrue($this->stockPairRepository->updateRows($attributes));
    }

    /**
     * @covers ::getStockPair
     * @covers ::__construct
     */
    public function testGetStockPair(): void
    {
        $condition = ['is_active' => ACTIVE_STATUS_ACTIVE];

        $this->stockPairModel
            ->shouldReceive('where')
            ->with($condition)
            ->once()
            ->andReturn($this->stockPairModel);

        $this->stockPairModel
            ->shouldReceive('leftJoin')
            ->with('stock_items as base_item', 'base_item.id', '=', 'stock_pairs.base_item_id')
            ->once()
            ->andReturn($this->stockPairModel);

        $this->stockPairModel
            ->shouldReceive('leftJoin')
            ->with('stock_items as stock_item', 'stock_item.id', '=', 'stock_pairs.stock_item_id')
            ->once()
            ->andReturn($this->stockPairModel);

        $this->stockPairModel
            ->shouldReceive('select')
            ->with([
                'stock_pairs.id as id',
                'stock_item.id as stock_item_id',
                'stock_item.item as stock_item_abbr',
                'stock_item.item_name as stock_item_name',
                'stock_item.item_type as stock_item_type',
                'base_item.id as base_item_id',
                'base_item.item as base_item_abbr',
                'base_item.item_name as base_item_name',
                'base_item.item_type as base_item_type',
                'last_price',
                'exchange_24',
                'stock_pairs.base_item_buy_order_volume',
                'stock_pairs.stock_item_buy_order_volume',
                'stock_pairs.base_item_sale_order_volume',
                'stock_pairs.stock_item_sale_order_volume',
                'stock_pairs.exchanged_buy_total',
                'stock_pairs.exchanged_sale_total',
                'stock_pairs.exchanged_amount',
                'stock_pairs.exchanged_maker_total',
                'stock_pairs.exchanged_buy_fee',
                'stock_pairs.exchanged_sale_fee',
                'stock_pairs.is_active',
                'stock_pairs.is_default',
                'stock_pairs.created_at',
            ])
            ->once()
            ->andReturn($this->stockPairModel);

        $this->assertSame($this->stockPairModel, $this->stockPairRepository->getStockPair($condition));
    }

    /**
     * @covers ::__construct
     * @covers ::getFirstStockPairDetailByConditions
     * @covers ::generateExchangeSummary
     */
    public function testGetFirstStockPairDetailByConditions(): void
    {
        $condition = ['is_active' => ACTIVE_STATUS_ACTIVE];

        $this->executeGetStockPair($condition);

        $stockPair = new StockPair();

        $this->stockPairModel->shouldReceive('first')->withNoArgs()->once()->andReturn($stockPair);

        $timestamp = Carbon::now()->subDay()->timestamp;

        $time1 = $timestamp - 1;
        $time2 = $timestamp + 2;
        $time3 = $timestamp + 1;

        $exchange24 = [
            $time1 => [
                'price' => $this->faker->randomNumber(4),
                'amount' => $this->faker->randomNumber(3),
                'total' => $this->faker->randomNumber(5),
            ],
            $time2 => [
                'price' => $this->faker->randomNumber(4),
                'amount' => $this->faker->randomNumber(3),
                'total' => $this->faker->randomNumber(5),
            ],
            $time3 => [
                'price' => $this->faker->randomNumber(4),
                'amount' => $this->faker->randomNumber(3),
                'total' => $this->faker->randomNumber(5),
            ],
        ];

        $exchange24Json = json_encode($exchange24);
        $stockPair->exchange_24 = $exchange24Json;

        $this->stockPairRepository->getFirstStockPairDetailByConditions($condition);

        unset($exchange24[$time1]);

        $this->assertSame(
            array_sum(array_column($exchange24, 'amount')),
            $stockPair->exchanged_stock_item_volume_24
        );

        $this->assertSame(
            array_sum(array_column($exchange24, 'total')),
            $stockPair->exchanged_base_item_volume_24
        );

        $this->assertSame(
            max(array_column($exchange24, 'price')),
            $stockPair->high_24
        );

        $this->assertSame(
            min(array_column($exchange24, 'price')),
            $stockPair->low_24
        );

        $firstPrice = array_first($exchange24)['price'];
        $lastPrice = array_last($exchange24)['price'];

        $this->assertSame(
            bcmul(bcdiv(bcsub($lastPrice, $firstPrice), $firstPrice), '100'),
            $stockPair->change_24
        );
    }

    /**
     * @covers ::getAllStockPairDetailByConditions
     */
    public function testGetAllStockPairDetailByConditions(): void
    {
        $condition = ['is_active' => ACTIVE_STATUS_ACTIVE];
        $stockPair = new StockPair();

        $collection = collect([$stockPair]);

        $this->executeGetStockPair($condition);

        $this->stockPairModel
            ->shouldReceive('get')
            ->once()
            ->withNoArgs()
            ->andReturn($collection);

        $this->assertSame($collection, $this->stockPairRepository->getAllStockPairDetailByConditions($condition));
    }

    public function testGetByPair(): void
    {
        $stockItem = $this->faker->word;
        $baseItem = $this->faker->word;

        $select = ['stock_pairs.*'];
        $where = [
            'stock_pairs.is_active' => ACTIVE_STATUS_ACTIVE,
            'si_stock.item' => $stockItem,
            'si_stock.is_active' => ACTIVE_STATUS_ACTIVE,
            'si_base.item' => $baseItem,
            'si_base.is_active' => ACTIVE_STATUS_ACTIVE,
        ];

        $this->stockPairModel->shouldReceive('select')->with($select)->once()->andReturn($this->stockPairModel);

        $this->stockPairModel->shouldReceive('where')->with($where)->once()->andReturn($this->stockPairModel);

        $this->stockPairModel
            ->shouldReceive('leftJoin')
            ->with('stock_items as si_stock', 'si_stock.id', '=', 'stock_pairs.stock_item_id')
            ->once()
            ->andReturn($this->stockPairModel);

        $this->stockPairModel
            ->shouldReceive('leftJoin')
            ->with('stock_items as si_base', 'si_base.id', '=', 'stock_pairs.base_item_id')
            ->once()
            ->andReturn($this->stockPairModel);

        $stockPair = new StockPair();

        $this->stockPairModel->shouldReceive('first')->withNoArgs()->once()->andReturn($stockPair);

        $this->assertSame($stockPair, $this->stockPairRepository->getByPair($stockItem, $baseItem));
    }

    public function testGetAllStockPairForApiByConditions(): void
    {
        $condition = ['is_active' => ACTIVE_STATUS_ACTIVE];

        $stockPair = new StockPair();
        $stockPair->stock_item_abbr = $this->faker->word;
        $stockPair->base_item_abbr = $this->faker->word;
        $this->executeGetStockPair($condition);

        $stockPairs = collect([$stockPair]);

        $this->stockPairModel
            ->shouldReceive('get')
            ->withNoArgs()
            ->once()
            ->andReturn($stockPairs);

        $stockPairDetails = $this->stockPairRepository->getAllStockPairForApiByConditions($condition);

        $arrayKey = sprintf('%s_%s', $stockPair->stock_item_abbr, $stockPair->base_item_abbr);

        $this->assertTrue(isset($stockPairDetails[$arrayKey]));

        $this->assertSame(
            array_keys($stockPairDetails[$arrayKey]),
            ['last', 'percentChange', 'baseVolume', 'coinVolume', 'high24hr', 'low24hr']
        );
    }

    /**
     * @param array $condition
     */
    private function executeGetStockPair(array $condition = []): void
    {
        $this->stockPairModel->shouldReceive('where')->with($condition)->once()->andReturn($this->stockPairModel);
        $this->stockPairModel->shouldReceive('leftJoin')->twice()->andReturn($this->stockPairModel);
        $this->stockPairModel->shouldReceive('select')->once()->andReturn($this->stockPairModel);
    }
}
