<?php

namespace Tests\Unit\Repositories\User\Eloquent;

use App\Models\Backend\Post;
use App\Models\User\Comment;
use App\Repositories\User\Eloquent\CommentRepository;
use Mockery\MockInterface;
use Tests\TestCase;

/**
 * @coversDefaultClass \App\Repositories\User\Eloquent\CommentRepository
 */
class CommentRepositoryTest extends TestCase
{
    /** @var Comment|MockInterface */
    private $commentModel;

    /** @var CommentRepository */
    private $commentRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->commentModel = $this->mock(Comment::class);

        $this->commentRepository = new CommentRepository($this->commentModel);
    }

    /**
     * @covers ::__construct
     * @covers ::save
     */
    public function testSave(): void
    {
        $post = $this->mock(Post::class);

        $attributes = ['is_published' => true];

        $post->shouldReceive('comments')->withNoArgs()->once()->andReturn($post);
        $post->shouldReceive('create')->with($attributes)->once()->andReturn($post);

        $this->assertEquals($post, $this->commentRepository->save($attributes, $post));
    }
}
