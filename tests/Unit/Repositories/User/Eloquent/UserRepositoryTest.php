<?php

namespace Tests\Unit\Repositories\User\Eloquent;

use App\Models\User\User;
use App\Repositories\User\Eloquent\UserRepository;
use Mockery\MockInterface;
use Tests\TestCase;

/**
 * @coversDefaultClass \App\Repositories\User\Eloquent\UserRepository
 */
class UserRepositoryTest extends TestCase
{
    /** @var User|MockInterface */
    private $userModel;

    /** @var UserRepository */
    private $userRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->userModel = $this->mock(User::class);

        $this->userRepository = new UserRepository($this->userModel);
    }

    /**
     * @covers ::__construct
     * @covers ::getCountByConditions
     */
    public function testGetCountByConditions(): void
    {
        $conditions = ['is_active' => true];

        $this->userModel
            ->shouldReceive('where')
            ->with($conditions)
            ->once()
            ->andReturn($this->userModel);

        $count = 1;

        $this->userModel
            ->shouldReceive('count')
            ->withNoArgs()
            ->once()
            ->andReturn($count);

        $this->assertEquals($count, $this->userRepository->getCountByConditions($conditions));
    }

    /**
     * @covers ::__construct
     * @covers ::getByUserIds
     */
    public function testGetByUserIds(): void
    {
        $ids = [1, 2, 3];
        $conditions = ['is_active' => true];

        $this->userModel
            ->shouldReceive('whereIn')
            ->with('id', $ids)
            ->once()
            ->andReturn($this->userModel);

        $this->userModel
            ->shouldReceive('where')
            ->with($conditions)
            ->once()
            ->andReturn($this->userModel);

        $collection = collect([]);

        $this->userModel
            ->shouldReceive('get')
            ->withNoArgs()
            ->once()
            ->andReturn($collection);

        $this->assertEquals($collection, $this->userRepository->getByUserIds($ids, $conditions));
    }
}
