#!/bin/bash

set -eu

echo "Install dependencies..."
./scripts/install-dependencies.sh

echo "Install pre-commit hook..."
./scripts/install-pre-commit.sh

if [[ ! -f ".env" ]]; then
    echo ".env does not exist. Copy .env.template"
    cp .env.template .env
fi

DEFAULT_DOCKER_WEB_HOST=wex.local

read -p "Do you wish to add new local host? [y/n]" YN
case ${YN} in
    [Yy]* )
        read -p "Please type host. Default: ${DEFAULT_DOCKER_WEB_HOST}: " DOCKER_WEB_HOST
        echo "You typed: ${DOCKER_WEB_HOST}"

        if [[ ${DOCKER_WEB_HOST} != '' ]]; then
            sudo ./scripts/add-host.sh ${DOCKER_WEB_HOST}
        else
            sudo ./scripts/add-host.sh ${DEFAULT_DOCKER_WEB_HOST}
        fi
        echo "New local host has been added"
        ;;
    [Nn]* ) echo 'Continue without new host...';;
esac

echo "Starting docker containers...";
./docker/start.sh
sleep 15

# show running containers
WEX_RUNNING_CONTAINERS="docker ps -f name=wex"
$WEX_RUNNING_CONTAINERS

if [[ $($WEX_RUNNING_CONTAINERS | wc -l) < 6 ]]; then
    echo "WEX containers should be at least 6, check that the installation was successful."
else
    echo "Everything is okay"
fi

# init app key
docker exec -it wex_app sh -c 'sed -i "/APP_KEY=/c\APP_KEY=$(php artisan key:generate --show)" .env'
docker exec -it wex_app php artisan config:clear

# execute migrations
read -p "Do you want to run migrations? [y/n]: " YN
case ${YN} in
    [Yy]* )
         read -p "Please type db user: " DB_USERNAME
         read -p "Please type db password: " DB_PASSWORD

         while ! docker exec wex_db mysqladmin --user=$DB_PASSWORD --password=$DB_USERNAME --host "127.0.0.1" ping --silent &> /dev/null ; do
            echo "Waiting for database connection..."
            sleep 2
         done

         docker exec -it wex_app php artisan migrate

         read -p "Do you want to install seeds? [y/n]" YN
         case ${YN} in
             [Yy]* )
                  docker exec -it wex_app php artisan db:seed
                 ;;
             [Nn]* ) echo 'Seeds are skipped';;
         esac
         ;;
    [Nn]* ) echo 'Installed without migrations';;
esac

# clear cache
docker exec -it wex_app php artisan clear:all


