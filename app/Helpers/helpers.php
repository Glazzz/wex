<?php

use App\Repositories\Core\Interfaces\AdminSettingInterface;
use App\Repositories\Core\Interfaces\SystemNoticeInterface;
use App\Repositories\Core\Interfaces\UserRoleManagementInterface;
use App\Repositories\User\Interfaces\NotificationInterface;
use App\Repositories\User\Interfaces\UserInterface;
use App\Services\Core\NavService;
use App\Services\User\ProfileService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

if (!function_exists('secret_env')) {
    /**
     * Use a docker secret value by name
     *
     * @param string $name
     * @param string|null $default
     *
     * @return string|null
     */
    function secret_env(string $name, ?string $default = null): ?string
    {
        $envValue = env($name);

        if (empty($envValue) || strpos($envValue, 'secret:') !== false) {
            static $secrets;

            $name = str_replace('secret:', '', $name);

            if (!isset($secrets[$name])) {
                $secretPath = sprintf('/run/secrets/%s', $name);

                if (file_exists($secretPath)) {
                    $secrets[$name] = trim(file_get_contents($secretPath));
                } else {
                    $secrets[$name] = $default;
                }
            }

            return $secrets[$name];
        }

        return $envValue;
    }
}

if (!function_exists('company_name')) {
    function company_name()
    {
        $companyName = admin_settings('company_name');

        return isset($companyName) && !empty($companyName) ? $companyName : env('APP_NAME');
    }
}

if (!function_exists('random_string')) {
    function random_string(int $length = 10, ?string $characters = null): string
    {
        if (empty($characters)) {
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        }
        $output = '';
        for ($i = 0; $i < $length; ++$i) {
            $y = random_int(0, strlen($characters) - 1);
            $output .= $characters[$y];
        }

        return $output;
    }
}

if (!function_exists('admin_settings')) {
    function admin_settings($admin_setting_field = null, $database = false)
    {
        if ($database) {
            if (null === $admin_setting_field) {
                $adminSettings = app(AdminSettingInterface::class)->getAll();
                $adminSettings = $adminSettings->pluck('value', 'slug')->toArray();
                foreach ($adminSettings as $key => $val) {
                    if (is_json($val)) {
                        $arrayConfig[$key] = json_decode($val, true);
                    } else {
                        $arrayConfig[$key] = $val;
                    }
                }
            } elseif (is_array($admin_setting_field) && count($admin_setting_field) > 0) {
                $arrayConfig = app(AdminSettingInterface::class)->getBySlugs($admin_setting_field);
            } else {
                $arrayConfig = app(AdminSettingInterface::class)->getBySlug($admin_setting_field);
            }

            return $arrayConfig;
        }

        $arrayConfig = cache()->get('admin_settings');

        if (is_array($arrayConfig)) {
            if (null !== $admin_setting_field) {
                if (is_array($admin_setting_field) && count($admin_setting_field) > 0) {
                    return array_intersect_key($arrayConfig, array_flip($admin_setting_field));
                } elseif (!is_array($admin_setting_field) && isset($arrayConfig[$admin_setting_field])) {
                    return $arrayConfig[$admin_setting_field];
                }

                return null;
            }

            return $arrayConfig;
        }

        return false;
    }
}

if (!function_exists('check_language')) {
    /**
     * @param string|null $language
     *
     * @return string|null
     */
    function check_language(?string $language)
    {
        $languages = language();
        if (array_key_exists($language, $languages)) {
            return $language;
        }

        return null;
    }
}

if (!function_exists('set_language')) {
    /**
     * @param string $language
     * @param string|null $default
     */
    function set_language(string $language, ?string $default = null): void
    {
        $languages = language();
        if (!isset($languages[$language])) {
            $language = request()->cookie('lang');

            if (!isset($languages[$language]) || null === check_language($language)) {
                // Get default value
                $language = $default ?? admin_settings('lang');

                if (isset($languages[$language])) {
                    $minutesInMonth = 43800;
                    cookie()->queue(cookie('lang', $language, $minutesInMonth, '/'));
                } else {
                    $language = LANGUAGE_DEFAULT;
                }
            }
        }

        App()->setlocale($language);
    }
}

if (!function_exists('language')) {
    function language($language = null, $langType = 'json')
    {
        $directories = [];
        $path = base_path('resources/lang');
        if ('json' === $langType) {
            $path .= '/';
            $initial = opendir($path);
            if ($dh = $initial) {
                while (false !== ($file = readdir($dh))) {
                    if (7 === strlen($file) && '.json' === substr($file, 2)) {
                        $ab = substr($file, 0, 2);
                        $directories[$ab] = $ab;
                    }
                }
                closedir($dh);
            }
        } else {
            $initial = scandir($path);
            foreach ($initial as $init) {
                if ('.' !== $init && '..' !== $init && 2 === strlen($init) && true !== strpos($init, '.')) {
                    $directories[$init] = $init;
                }
            }
        }

        return null === $language ? $directories : $directories[$language];
    }
}

if (!function_exists('fake_field')) {
    function fake_field($fieldname, $reverse = false)
    {
        if (true === $reverse) {
            return array_flip(config('fakefields.table_keys'))[$fieldname];
        }

        return config()->get('fakefields.table_keys.'.$fieldname, $fieldname);
    }
}

if (!function_exists('base_key')) {
    function base_key(): string
    {
        return encode_decode(config('fakefields.base_key'));
    }
}

if (!function_exists('encode_decode')) {
    function encode_decode(string $data, bool $decryption = false)
    {
        $code = ['x', 'f', 'z', 's', 'b', 'h', 'n', 'a', 'c', 'm'];
        if ($decryption) {
            $code = array_flip($code);
        }
        $output = '';
        $length = strlen($data);
        try {
            for ($i = 0; $i < $length; ++$i) {
                $y = $data[$i];
                $output .= $code[$y];
            }
        } catch (\Exception $e) {
            $output = null;
        }

        return $output;
    }
}

if (!function_exists('validate_date')) {
    function validate_date(string $date, string $seperator = '-'): bool
    {
        $datepart = explode($seperator, $date);

        return 10 === strlen($date)
            && 3 === count($datepart)
            && 4 === strlen($datepart[0])
            && 2 === strlen($datepart[1])
            && 2 === strlen($datepart[2])
            && ctype_digit($datepart[0])
            && ctype_digit($datepart[1])
            && ctype_digit($datepart[2])
            && checkdate($datepart[1], $datepart[2], $datepart[0]);
    }
}

if (!function_exists('has_permission')) {
    function has_permission(string $routeName, int $userId = null, bool $is_link = true, bool $is_api = false): bool
    {
        $configPath = $is_api ? 'permissionApiRoutes' : 'permissionRoutes';
        $isAccessible = $is_link ? false : ROUTE_REDIRECT_TO_UNAUTHORIZED;
        if (null === $userId) {
            $user = auth()->user();
        } else {
            $user = app(UserInterface::class)->getFirstById($userId);
        }
        if (empty($user)) {
            config()->set($configPath.'.all_accessible_routes', []);

            return $isAccessible;
        }
        $allAccessibleRoutes = config($configPath.'.all_accessible_routes');
        $routeConfig = config($configPath);
        if (null === $allAccessibleRoutes) {
            $allAccessibleRoutes = [];
            $permissionGroups = cache()->get("userRoleManagement{$user->user_role_management_id}");
            if (null === $permissionGroups) {
                $permissionGroups = app(UserRoleManagementInterface::class)->getFirstById($user->user_role_management_id)->route_group;
                cache()->forever("userRoleManagement{$user->user_role_management_id}", $permissionGroups);
            }
            if (empty($permissionGroups)) {
                config()->set($configPath.'.all_accessible_routes', config($configPath.'.private_routes'));
            } else {
                foreach ($permissionGroups as $permissionGroupName => $permissionGroup) {
                    foreach ($permissionGroup as $groupName => $groupAccessName) {
                        foreach ($groupAccessName as $accessName) {
                            try {
                                $routes = $routeConfig['configurable_routes'][$permissionGroupName][$groupName][$accessName];
                                if (in_array($routeName, $routes)) {
                                    $isAccessible = true;
                                }
                                $allAccessibleRoutes = array_merge($allAccessibleRoutes, $routes);
                            } catch (\Exception $e) {
                            }
                        }
                    }
                }
                $allAccessibleRoutes = array_merge($allAccessibleRoutes, $routeConfig['private_routes']);
                config()->set($configPath.'.all_accessible_routes', $allAccessibleRoutes);
            }
        }
        if (UNDER_MAINTENANCE_MODE_ACTIVE == admin_settings('maintenance_mode') && UNDER_MAINTENANCE_ACCESS_ACTIVE != $user->is_accessible_under_maintenance) {
            if (
                !empty($allAccessibleRoutes) && in_array($routeName, $allAccessibleRoutes) &&
                in_array($routeName, $routeConfig['avoidable_maintenance_routes'])
            ) {
                $isAccessible = true;
            } else {
                $isAccessible = $is_link ? false : ROUTE_REDIRECT_TO_UNDER_MAINTENANCE;
            }
        } elseif (in_array($routeName, $routeConfig['private_routes'])) {
            $isAccessible = true;
        } elseif (!empty($allAccessibleRoutes) && in_array($routeName, $allAccessibleRoutes)) {
            if (in_array($routeName, $routeConfig['avoidable_unverified_routes'])) {
                $isAccessible = true;
            } elseif (in_array($routeName, $routeConfig['avoidable_suspended_routes'])) {
                $isAccessible = true;
            } elseif (in_array($routeName, $routeConfig['financial_routes'])) {
                if (FINANCIAL_STATUS_ACTIVE == $user->is_financial_active) {
                    $isAccessible = true;
                } else {
                    $isAccessible = $is_link ? false : ROUTE_REDIRECT_TO_FINANCIAL_ACCOUNT_SUSPENDED;
                }
            } elseif (
                (
                    EMAIL_VERIFICATION_STATUS_ACTIVE == $user->is_email_verified ||
                    ACTIVE_STATUS_INACTIVE == admin_settings('require_email_verification')
                ) && ACCOUNT_STATUS_ACTIVE == $user->is_active
            ) {
                $isAccessible = true;
            } else {
                if (EMAIL_VERIFICATION_STATUS_ACTIVE != $user->is_email_verified &&
                    ACTIVE_STATUS_ACTIVE == admin_settings('require_email_verification')) {
                    $isAccessible = $is_link ? false : ROUTE_REDIRECT_TO_EMAIL_UNVERIFIED;
                } elseif (ACCOUNT_STATUS_ACTIVE != $user->is_active) {
                    $isAccessible = $is_link ? false : ROUTE_REDIRECT_TO_ACCOUNT_SUSPENDED;
                }
            }
        }

        return $isAccessible;
    }
}

if (!function_exists('is_json')) {
    function is_json(string $string): bool
    {
        return is_string($string) && is_array(json_decode($string, true)) && (JSON_ERROR_NONE === json_last_error());
    }
}

if (!function_exists('is_current_route')) {
    /**
     * @param string $route_name
     * @param string $active_class_name
     * @param null $must_have_route_parameters
     * @param null $optional_route_parameters
     *
     * @return string
     */
    function is_current_route(
        $route_name,
        string $active_class_name = 'active',
        $must_have_route_parameters = null,
        $optional_route_parameters = null
    ) {
        if (!is_array($route_name)) {
            $is_selected = \Route::currentRouteName() === $route_name;
        } else {
            $is_selected = in_array(\Route::currentRouteName(), $route_name, true);
        }
        if ($is_selected) {
            if ($optional_route_parameters) {
                if (is_array($must_have_route_parameters)) {
                    $is_selected = available_in_parameters($must_have_route_parameters);
                }
                if (is_array($optional_route_parameters)) {
                    $is_selected = available_in_parameters($optional_route_parameters, true);
                }
            } elseif (is_array($must_have_route_parameters)) {
                $is_selected = available_in_parameters($must_have_route_parameters);
            }
        }

        return true === $is_selected ? $active_class_name : '';
    }

    /**
     * @param array $route_parameter
     * @param bool $optional
     *
     * @return bool
     */
    function available_in_parameters(array $route_parameter, bool $optional = false): bool
    {
        $is_selected = true;
        foreach ($route_parameter as $key => $val) {
            if (is_array($val)) {
                $current_route_parameter = \Request::route()->parameter($val[0]);
                if ('<' === $val[1]) {
                    $is_selected = $current_route_parameter < $val[2];
                } elseif ('<=' === $val[1]) {
                    $is_selected = $current_route_parameter <= $val[2];
                } elseif ('>' === $val[1]) {
                    $is_selected = $current_route_parameter > $val[2];
                } elseif ('>=' === $val[1]) {
                    $is_selected = $current_route_parameter >= $val[2];
                } elseif ('!=' === $val[1]) {
                    $is_selected = $current_route_parameter != $val[2];
                } else {
                    $param = $val[2] ?? $val[1];
                    $is_selected = $current_route_parameter == $param;
                }
            } else {
                $current_route_parameter = \Request::route()->parameter($key);
                if ($optional && 0 !== $current_route_parameter && empty($current_route_parameter)) {
                    continue;
                }
                $is_selected = $current_route_parameter == $val;
            }
            if (false === $is_selected) {
                break;
            }
        }

        return $is_selected;
    }
}

if (!function_exists('return_get')) {
    function return_get($key, $val = '')
    {
        $output = '';
        if (isset($_GET[$key]) && $_GET[$key] === (string) $val && '' !== $val) {
            $output = ' selected';
        } elseif (isset($_GET[$key]) && '' == $val) {
            $output = $_GET[$key];
        }

        return $output;
    }
}

if (!function_exists('array_to_string')) {
    function array_to_string(
        array $array,
        string $separator = ',',
        bool $key = true,
        bool $is_seperator_at_ends = false
    ) {
        if (true === $key) {
            $output = implode($separator, array_keys($array));
        } else {
            $output = implode($separator, array_values($array));
        }

        return $is_seperator_at_ends ? $separator.$output.$separator : $output;
    }
}

if (!function_exists('valid_image')) {
    function valid_image(string $imagePath, ?string $image): bool
    {
        $extension = explode('.', $image);
        $isExtensionAvailable = in_array(end($extension), config('commonconfig.image_extensions'), true);

        return $isExtensionAvailable && file_exists(public_path($imagePath.$image));
    }
}

if (!function_exists('get_avatar')) {
    function get_avatar(?string $avatar): string
    {
        $avatarPath = 'storage/'.config('commonconfig.path_profile_image');

        $avatar = valid_image($avatarPath, $avatar) ? $avatarPath.$avatar : $avatarPath.'avatar.jpg';

        return asset($avatar);
    }
}

if (!function_exists('get_item_emoji')) {
    function get_item_emoji(?string $image): ?string
    {
        $emojiPath = 'storage/'.config('commonconfig.path_stock_item_emoji');

        if (valid_image($emojiPath, $image)) {
            return asset($emojiPath.$image);
        }

        return null;
    }
}

if (!function_exists('get_id_image')) {
    function get_id_image(?string $image): ?string
    {
        $idCardPath = 'storage/'.config('commonconfig.path_id_image');
        if (valid_image($idCardPath, $image)) {
            return asset($idCardPath.$image);
        }

        return null;
    }
}

if (!function_exists('get_image')) {
    function get_image(?string $image): ?string
    {
        $imagePath = 'storage/'.config('commonconfig.path_image');
        if (valid_image($imagePath, $image)) {
            return asset($imagePath.$image);
        }

        return null;
    }
}

if (!function_exists('get_post_image')) {
    function get_post_image(?string $image): ?string
    {
        $postImage = 'storage/'.config('commonconfig.path_post');

        if (valid_image($postImage, $image)) {
            return asset($postImage.$image);
        }

        return null;
    }
}

if (!function_exists('get_user_specific_notice')) {
    function get_user_specific_notice(?int $userId = null): array
    {
        if (null === $userId) {
            $userId = Auth::id();
        }

        $notificationRepository = app(NotificationInterface::class);

        return [
            'list' => $notificationRepository->getLastFive($userId),
            'count_unread' => $notificationRepository->countUnread($userId),
        ];
    }
}

if (!function_exists('get_nav')) {
    function get_nav(string $slug, string $template = 'default_nav')
    {
        return app(NavService::class)->navigationSingle($slug, $template);
    }
}

if (!function_exists('get_breadcrumbs')) {
    function get_breadcrumbs(): array
    {
        $routeList = Route::getRoutes()->getRoutesByMethod()['GET'];
        $baseUrl = url('/');
        $segments = Request::segments();
        $routeUries = explode('/', Route::current()->uri());
        $breadcrumbs = [];

        foreach ($segments as $key => $segment) {
            $displayUrl = true;
            $lastBreadcrumb = end($breadcrumbs);
            if (empty($lastBreadcrumb)) {
                $url = $baseUrl.'/'.$segment;
            } else {
                $url = $lastBreadcrumb['url'].'/'.$segment;
            }

            if (!array_key_exists(implode('/', array_slice($routeUries, 0, $key + 1)), $routeList)) {
                $displayUrl = false;
            }
            $breadcrumbs[] = [
                'name' => title_case(str_replace('-', ' ', $segment)),
                'url' => $url,
                'display_url' => $displayUrl,
            ];
        }

        return $breadcrumbs;
    }
}

if (!function_exists('get_system_notices')) {
    function get_system_notices()
    {
        $systemNoticeInterface = app(SystemNoticeInterface::class);
        $date = Carbon::now();
        $totalMinutes = $date->diffInMinutes($date->copy()->endOfDay());

        if (Cache::has('systemNotices')) {
            $systemNotices = Cache::get('systemNotices');
        } else {
            $systemNotices = $systemNoticeInterface->todaysNotifications();
            Cache::put('systemNotices', $systemNotices, $totalMinutes);
        }

        if ($systemNotices->isEmpty()) {
            return $systemNotices;
        }

        $systemNoticeIds = $systemNotices->pluck('updated_at', 'id')->toArray();
        if (session()->has('seenSystemNotices')) {
            $seenSystemNotices = session()->get('seenSystemNotices');
            $systemNotices = $systemNotices->filter(function ($systemNotice, $key) use ($seenSystemNotices) {
                $date = now();
                if (!($systemNotice->start_at <= $date && $systemNotice->end_at >= $date)) {
                    return false;
                }

                if (
                    array_key_exists($systemNotice->id, $seenSystemNotices)
                    && $systemNotice->updated_at->eq($seenSystemNotices[$systemNotice->id])
                ) {
                    return false;
                }

                return $systemNotice;
            });
        }

        session()->put('seenSystemNotices', $systemNoticeIds);

        return $systemNotices;
    }
}

if (!function_exists('get_available_timezones')) {
    function get_available_timezones(): array
    {
        return [
            'UTC' => __('Default'),
            'BST' => __('Bangladesh Standard Time'),
        ];
    }
}

if (!function_exists('get_minimum_order_total')) {
    function get_minimum_order_total(string $minimumFee = MINIMUM_TRANSACTION_FEE_CRYPTO): string
    {
        $adminSettings = admin_settings(['exchange_maker_fee', 'exchange_taker_fee']);
        $comparison = bccomp($adminSettings['exchange_taker_fee'], $adminSettings['exchange_maker_fee']) > 0;
        $feeInPercentage = $comparison ? $adminSettings['exchange_maker_fee'] : $adminSettings['exchange_taker_fee'];

        if (bccomp('0', $feeInPercentage) >= 0) {
            return $minimumFee;
        }

        return bcdiv(bcmul('100', $minimumFee), $feeInPercentage);
    }
}

if (!function_exists('get_transaction_type')) {
    function get_transaction_type($key = null)
    {
        $transactionType = [
            TRANSACTION_TYPE_DEBIT => __('Debit'),
            TRANSACTION_TYPE_CREDIT => __('Credit'),
        ];

        return null === $key ? $transactionType : $transactionType[$key];
    }
}

if (!function_exists('calculate_exchange_fee')) {
    function calculate_exchange_fee($amount, $feeInPercentage): string
    {
        return bcdiv(bcmul($amount, $feeInPercentage), '100');
    }
}

if (!function_exists('profileRoutes')) {
    function profileRoutes($identifier, $userId)
    {
        $userService = app(ProfileService::class);
        if ('admin' === $identifier) {
            return $userService->routesForAdmin($userId);
        }

        return $userService->routesForUser($userId);
    }
}

if (!function_exists('social_media_link')) {
    function social_media_link($socialMediaName)
    {
        $fieldName = str_replace('_', '-', $socialMediaName);
        if (!empty(admin_settings($socialMediaName.'_link'))) {
            return '<li><a href="'.admin_settings($socialMediaName.'_link').'"><i class="fa fa-'.strtolower($fieldName).'-square font-20"></i></a></li>';
        }
    }
}

if (!function_exists('channel_prefix')) {
    function channel_prefix(): string
    {
        return str_slug(env('APP_NAME', 'laravel'), '_').'_';
    }
}
