<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Models\Audit;

/**
 * App\Models\Core\SystemNotice
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $type
 * @property \Carbon\Carbon $start_at
 * @property \Carbon\Carbon $end_at
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Collection|Audit[] $audits
 * @method Builder|SystemNotice newModelQuery()
 * @method Builder|SystemNotice newQuery()
 * @method static Builder|SystemNotice query()
 * @mixin \Eloquent
 */
class SystemNotice extends Model implements AuditableInterface
{
    use Auditable;

    protected $fillable = [
        'title',
        'description',
        'type',
        'start_at',
        'end_at',
        'status',
    ];

    protected $fakeFields = [
        'title',
        'description',
        'type',
        'start_at',
        'end_at',
        'status',
    ];
}
