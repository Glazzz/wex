<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Models\Audit;

/**
 * App\Models\Core\Navigation
 *
 * @property int $id
 * @property string $slug
 * @property string $navigation_items
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Collection|Audit[] $audits
 * @property mixed $navigation_items
 * @method Builder|Navigation newModelQuery()
 * @method Builder|Navigation newQuery()
 * @method static Builder|Navigation query()
 * @mixin \Eloquent
 */
class Navigation extends Model implements AuditableInterface
{
    use Auditable;

    protected $fillable = [
        'slug',
        'navigation_items',
    ];

    protected $fakeFields = [
        'slug',
        'navigation_items',
    ];

    /**
     * @param string $value
     * @return array
     */
    public function getNavigationItemsAttribute(string $value): array
    {
        return \json_decode($value, true);
    }

    public function setNavigationItemsAttribute(array $value)
    {
        return $this->attributes['navigation_items'] = \json_encode($value);
    }
}
