<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Models\Audit;

/**
 * App\Models\Core\AdminSetting
 *
 * @property int $id
 * @property string $slug
 * @property string $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Collection|Audit[] $audits
 * @property-read mixed $route_group
 * @method Builder|AdminSetting newModelQuery()
 * @method Builder|AdminSetting newQuery()
 * @method static Builder|AdminSetting query()
 * @mixin \Eloquent
 */
class AdminSetting extends Model implements AuditableInterface
{
    use Auditable;

    protected $fillable = [
        'slug',
        'value',
    ];

    /**
     * @param string $value
     * @return array
     */
    public function getRouteGroupAttribute(string $value): array
    {
        return \json_decode($value,true);
    }
}
