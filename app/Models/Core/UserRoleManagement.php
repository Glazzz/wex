<?php

namespace App\Models\Core;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Models\Audit;

/**
 * App\Models\Core\UserRoleManagement
 *
 * @property int $id
 * @property string $tole_name
 * @property string $route_group
 * @property bool $is_active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Collection|Audit[] $audits
 * @property mixed $route_group
 * @property-read Collection|\App\Models\User\User[] $users
 * @method Builder|UserRoleManagement newModelQuery()
 * @method Builder|UserRoleManagement newQuery()
 * @method static Builder|UserRoleManagement query()
 * @mixin \Eloquent
 */
class UserRoleManagement extends Model implements AuditableInterface
{
    use Auditable;

    protected $fillable = [
        'role_name',
        'route_group',
    ];

    protected $fakeFields = [
        'role_name',
        'route_group',
    ];

    /**
     * @param string $value
     * @return array
     */
    public function getRouteGroupAttribute(string $value): array
    {
        return json_decode($value, true);
    }

    /**
     * @param array $value
     * @return UserRoleManagement
     */
    public function setRouteGroupAttribute(array $value): self
    {
        $this->attributes['route_group'] = \json_encode($value);

        return $this;
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
