<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Models\Audit;

/**
 * App\Models\User\UserSetting
 *
 * @property int $id
 * @property int $user_id
 * @property string $language
 * @property string $timezone
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Collection|Audit[] $audits
 * @property-read \App\Models\User\User $user
 * @method Builder|Deposit newModelQuery()
 * @method Builder|Deposit newQuery()
 * @method static Builder|Deposit query()
 * @mixin \Eloquent
 */
class UserSetting extends Model implements AuditableInterface
{
    use Auditable;

    protected $fillable = [
        'user_id',
        'language',
        'timezone',
    ];

    protected $fakeFields = [
        'language',
        'timezone',
        'is_otp_allowed',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
