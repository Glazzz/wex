<?php

namespace App\Models\User;

use App\Models\Backend\StockItem;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User\ReferralEarning
 *
 * @property int $id
 * @property-read User $referralUser
 * @property-read User $referrerUser
 * @property-read StockItem $stockItem
 * @method Builder|self newModelQuery()
 * @method Builder|self newQuery()
 * @method static Builder|self query()
 * @mixin \Eloquent
 */
class ReferralEarning extends Model
{
    protected $guarded = ['id'];

    public function referrerUser()
    {
        return $this->belongsTo(User::class, 'referrer_user_id');
    }

    public function referralUser()
    {
        return $this->belongsTo(User::class, 'referral_user_id');
    }

    public function stockItem()
    {
        return $this->belongsTo(StockItem::class);
    }
}
