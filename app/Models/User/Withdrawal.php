<?php

namespace App\Models\User;

use App\Models\Backend\StockItem;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User\Withdrawal
 *
 * @property int $id
 * @property int $user_id
 * @property string $ref_id
 * @property int $wallet_id
 * @property int $stock_item_id
 * @property float $amount
 * @property float $network_fee
 * @property float $system_fee
 * @property string $address
 * @property string $txn_id
 * @property int $payment_method
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Backend\StockItem $stockItem
 * @property-read \App\Models\User\User $user
 * @property-read \App\Models\User\Wallet $wallet
 * @method Builder|self newModelQuery()
 * @method Builder|self newQuery()
 * @method static Builder|self query()
 * @mixin \Eloquent
 */
class Withdrawal extends Model
{
    protected $fillable = [
        'user_id',
        'ref_id',
        'wallet_id',
        'stock_item_id',
        'amount',
        'network_fee',
        'system_fee',
        'address',
        'txn_id',
        'payment_method',
        'status'
    ];

    public function stockItem()
    {
        return $this->belongsTo(StockItem::class);
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
