<?php

namespace App\Models\User;

use App\Models\Core\UserRoleManagement;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

/**
 * App\Models\User\UserInfo
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $address
 * @property string $phone
 * @property bool $is_id_verified
 * @property int $id_type
 * @property int $id_card_front
 * @property int $id_card_back
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $fullName
 * @property User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read mixed $full_name
 * @method Builder|UserInfo newModelQuery()
 * @method Builder|UserInfo newQuery()
 * @method static Builder|UserInfo query()
 * @mixin \Eloquent
 */
class UserInfo extends Model implements AuditableInterface
{
    use Auditable;

    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'address',
        'phone',
        'is_id_verified',
        'id_type',
        'id_card_front',
        'id_card_back',
    ];

    protected $fakeFields = [
        'user_id',
        'first_name',
        'last_name',
        'country_id',
        'address',
        'phone',
        'is_id_verified',
        'id_type',
        'id_card_front',
        'id_card_back',
    ];

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}