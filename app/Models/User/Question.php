<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User\Question
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Collection|Comment[] $comments
 * @property-read User $user
 * @method Builder|self newModelQuery()
 * @method Builder|self newQuery()
 * @method static Builder|self query()
 * @mixin \Eloquent
 */
class Question extends Model
{
    protected $fillable = [
        'user_id',
        'title',
        'content',
    ];

    protected $fakeFields = [
        'title',
        'content',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
