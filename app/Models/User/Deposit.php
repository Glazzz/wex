<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User\Deposit
 *
 * @property int $id
 * @property int $user_id
 * @property string $ref_id
 * @property int $wallet_id
 * @property int $stick_item_id
 * @property float $amount
 * @property float $network_fee
 * @property float $system_fee
 * @property string $address
 * @property string $txn_id
 * @property int $payment_method
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method Builder|Deposit newModelQuery()
 * @method Builder|Deposit newQuery()
 * @method static Builder|Deposit query()
 * @mixin \Eloquent
 */
class Deposit extends Model
{
    protected $fillable = [
        'user_id',
        'ref_id',
        'wallet_id',
        'stock_item_id',
        'amount',
        'network_fee',
        'system_fee',
        'address',
        'txn_id',
        'payment_method',
        'status',
    ];
}
