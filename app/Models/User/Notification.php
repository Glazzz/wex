<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Models\Audit;

/**
 * App\Models\User\Notification
 *
 * @property int $id
 * @property int $user_id
 * @property string $data
 * @property \Carbon\Carbon $read_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Collection|Audit[] $audits
 * @method Builder|self newModelQuery()
 * @method Builder|self newQuery()
 * @method static Builder|self query()
 * @mixin \Eloquent
 */
class Notification extends Model implements AuditableInterface
{
    use Auditable;

    protected $fillable = [
        'user_id',
        'data',
        'read_at',
    ];

    protected $fakeFields = [
        'user_id',
        'data',
        'read_at',
    ];
}
