<?php

namespace App\Models\User;

use App\Models\Backend\StockItem;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User\Wallet
 *
 * @property int $id
 * @property int $user_id
 * @property int $stock_item_id
 * @property float $primary_balance
 * @property float $on_order_balance
 * @property string $address
 * @property bool $is_active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Backend\StockItem $stockItem
 * @method Builder|Wallet newModelQuery()
 * @method Builder|Wallet newQuery()
 * @method static Builder|Wallet query()
 * @mixin \Eloquent
 */
class Wallet extends Model
{
    protected $fillable = [
        'id',
        'user_id',
        'stock_item_id',
        'primary_balance',
        'on_order_balance',
        'address',
        'is_active',
    ];

    public function stockItem()
    {
        return $this->belongsTo(StockItem::class);
    }
}
