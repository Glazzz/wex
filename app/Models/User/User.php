<?php

namespace App\Models\User;

use App\Models\Core\UserRoleManagement;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

/**
 * App\Models\User\User
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property bool $remember_me
 * @property int user_role_management_id
 * @property string $avatar
 * @property bool $is_email_verified
 * @property bool $is_financial_active
 * @property bool $is_accessible_under_maintenance
 * @property string $google2fa_secret
 * @property bool $is_active
 * @property bool $created_by_admin
 * @property string $referral_code
 * @property int $referrer_id
 * @property string $created_at
 * @property string $updated_at
 * @property-read Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read UserInfo $userInfo
 * @property-read UserRoleManagement $userRoleManagement
 * @property-read UserSetting $userSetting
 * @property-read Collection|Wallet[] $wallets
 * @method Builder|User newModelQuery()
 * @method Builder|User newQuery()
 * @method static Builder|User query()
 * @mixin \Eloquent
 */
class User extends Authenticatable implements AuditableInterface
{
    use Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'password',
        'email',
        'user_role_management_id',
        'remember_me',
        'avatar',
        'is_email_verified',
        'is_financial_active',
        'is_accessible_under_maintenance',
        'google2fa_secret',
        'is_active',
        'created_by_admin',
        'referral_code',
        'referrer_id',
    ];

    protected $fakeFields = [
        'username',
        'password',
        'email',
        'user_role_management_id',
        'remember_me',
        'avatar',
        'is_email_verified',
        'is_financial_active',
        'is_accessible_under_maintenance',
        'google2fa_secret',
        'is_active',
        'created_by_admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function userRoleManagement()
    {
        return $this->belongsTo(UserRoleManagement::class);
    }

    public function userInfo()
    {
        return $this->hasOne(UserInfo::class);
    }

    public function userSetting()
    {
        return $this->hasOne(UserSetting::class);
    }

    public function wallets()
    {
        return $this->hasMany(Wallet::class);
    }
}
