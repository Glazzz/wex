<?php

namespace App\Models\User;

use App\Models\Backend\StockPair;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User\StockOrder
 *
 * @property int $id
 * @property int $user_id
 * @property int $stock_pair_id
 * @property int $category
 * @property int $exchange_type
 * @property float $price
 * @property float $amount
 * @property float $exchanged
 * @property float $canceled
 * @property float $stop_limit
 * @property float $maker_fee
 * @property float $taker_fee
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read StockPair $stockPair
 * @property-read User $user
 * @method Builder|self newModelQuery()
 * @method Builder|self newQuery()
 * @method static Builder|self query()
 * @mixin \Eloquent
 */
class StockOrder extends Model
{
    protected $fillable = [
        'user_id',
        'stock_pair_id',
        'category',
        'exchange_type',
        'status',
        'price',
        'amount',
        'exchanged',
        'canceled',
        'stop_limit',
        'maker_fee',
        'taker_fee',
    ];

    protected $fakeFields = [
        'user_id',
        'stock_pair_id',
        'category',
        'exchange_type',
        'status',
        'price',
        'amount',
        'exchanged',
        'canceled',
        'stop_limit',
        'maker_fee',
        'taker_fee',
    ];

    public function stockPair()
    {
        return $this->belongsTo(StockPair::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
