<?php

namespace App\Models\Exchange;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Exchange\StockExchangeGroup
 *
 * @property int $id
 * @method Builder|StockExchangeGroup newModelQuery()
 * @method Builder|StockExchangeGroup newQuery()
 * @method static Builder|StockExchangeGroup query()
 * @mixin \Eloquent
 */
class StockExchangeGroup extends Model
{
    //
}
