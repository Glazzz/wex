<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Backend\Transaction
 *
 * @property int $id
 * @property int $user_id
 * @property int $stock_item_id
 * @property string $model_name
 * @property int $model_id
 * @property int $transaction_type
 * @property float $about
 * @property int $journal
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method Builder|Transaction newModelQuery()
 * @method Builder|Transaction newQuery()
 * @method static Builder|Transaction query()
 * @mixin \Eloquent
 */
class Transaction extends Model
{
    protected $fillable = [
        'user_id',
        'stock_item_id',
        'table_name',
        'row_id',
        'transaction_type',
        'amount',
    ];
}
