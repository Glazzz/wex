<?php

namespace App\Models\Backend;

use App\Models\User\Comment;
use App\Models\User\Deposit;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Backend\Post
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $content
 * @property string $featured_image
 * @property int $is_published
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Collection|\App\Models\User\Comment[] $comments
 * @property-read \App\Models\User\User $user
 * @method Builder|Deposit newModelQuery()
 * @method Builder|Deposit newQuery()
 * @method static Builder|Deposit query()
 * @mixin \Eloquent
 */
class Post extends Model
{
    protected $fillable = [
        'user_id',
        'title',
        'content',
        'featured_image',
        'is_published',
    ];

    protected $fakeFields = [
        'title',
        'content',
        'featured_image',
        'is_published',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}