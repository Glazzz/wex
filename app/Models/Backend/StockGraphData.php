<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Backend\StockGraphData
 *
 * @property int $id
 * @property int $stock_pair_id
 * @property string $5min
 * @property string $15min
 * @property string $30min
 * @property string $2hr
 * @property string $4hr
 * @property string $1day
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method Builder|StockGraphData newModelQuery()
 * @method Builder|StockGraphData newQuery()
 * @method static Builder|StockGraphData query()
 * @mixin \Eloquent
 */
class StockGraphData extends Model
{
    protected $fillable = [
        'stock_pair_id',
        '5min',
        '15min',
        '30min',
        '2hr',
        '4hr',
        '1day',
        'created_at',
        'updated_at',
    ];
}
