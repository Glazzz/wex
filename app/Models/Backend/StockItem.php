<?php

namespace App\Models\Backend;

use App\Models\Backend\StockPair;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Models\Audit;

/**
 * App\Models\Backend\StockItem
 *
 * @property int $id
 * @property string $item
 * @property string $item_name
 * @property int $item_type
 * @property string $item_emoji
 * @property bool $is_active
 * @property int $exchange_status
 * @property int $is_fee_applicable
 * @property int $is_ico
 * @property int $deposit_status
 * @property float $deposit_fee
 * @property int $withdrawal_status
 * @property float $withdrawal_fee
 * @property float $minimum_withdrawal_amount
 * @property float $daily_withdrawal_limit
 * @property int $api_service
 * @property float $total_withdrawal
 * @property float $total_withdrawal_fee
 * @property float $total_deposit
 * @property float $total_deposit_fee
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Collection|Audit[] $audits
 * @property-read Collection|StockPair[] $baseStockPairs
 * @property-read string $stock_item_name
 * @property-read Collection|StockPair[] $stockPairs
 * @method Builder|StockItem newModelQuery()
 * @method Builder|StockItem newQuery()
 * @method static Builder|StockItem query()
 * @mixin \Eloquent
 */
class StockItem extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'item',
        'item_name',
        'item_type',
        'item_emoji',
        'is_active',
        'exchange_status',
        'is_ico',
        'deposit_status',
        'deposit_fee',
        'withdrawal_status',
        'withdrawal_fee',
        'minimum_withdrawal_amount',
        'daily_withdrawal_limit',
        'api_service',
        'total_deposit',
        'total_pending_deposit',
        'total_deposit_fee',
        'total_withdrawal',
        'total_pending_withdrawal',
        'total_withdrawal_fee',
    ];

    protected $fakeFields = [
        'item',
        'item_name',
        'item_type',
        'item_emoji',
        'is_active',
        'exchange_status',
        'is_ico',
        'deposit_status',
        'deposit_fee',
        'withdrawal_status',
        'withdrawal_fee',
        'minimum_withdrawal_amount',
        'daily_withdrawal_limit',
        'api_service',
        'total_deposit',
        'total_pending_deposit',
        'total_deposit_fee',
        'total_withdrawal',
        'total_pending_withdrawal',
        'total_withdrawal_fee',
        ];

    public function getStockItemNameAttribute(): string
    {
        return $this->item . ' (' . $this->item_name . ')';
    }

    public function baseStockPairs()
    {
        return $this->hasMany(StockPair::class,'base_item_id');
    }

    public function stockPairs()
    {
        return $this->hasMany(StockPair::class,'stock_item_id');
    }
}