<?php

namespace App\Models\Backend;

use App\Models\User\StockOrder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Backend\StockExchange
 *
 * @property int $id
 * @property $int $user_id
 * @property int $stock_exchange_group_id
 * @property int $stock_order_id
 * @property int $stock_pair_id
 * @property float $amount
 * @property float $price
 * @property float $fee
 * @property float $referral_earning
 * @property int $exchange_type
 * @property int $related_order_id
 * @property int $is_maker
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\User\StockOrder $stockOrder
 * @method Builder|StockExchange newModelQuery()
 * @method Builder|StockExchange newQuery()
 * @method static Builder|StockExchange query()
 * @mixin \Eloquent
 */
class StockExchange extends Model
{
    protected $fillable = [
        'user_id',
        'stock_exchange_group_id',
        'stock_order_id',
        'stock_pair_id',
        'amount',
        'price',
        'total',
        'fee',
        'referral_earning',
        'exchange_type',
        'related_order_id',
        'base_order',
        'is_maker',
    ];

    public function stockOrder()
    {
        return $this->belongsTo(StockOrder::class);
    }
}
