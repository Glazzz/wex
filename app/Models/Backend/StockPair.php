<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Backend\StockPair
 *
 * @property int $id
 * @property int $stock_item_id
 * @property int $base_item_id
 * @property int $is_active
 * @property int $is_default
 * @property string $exchange_24
 * @property float $last_price
 * @property float $base_item_buy_order_volume
 * @property float $stock_item_buy_order_volume
 * @property float $base_item_sale_order_volume
 * @property float $stock_item_sale_order_volume
 * @property float $exchanged_buy_total
 * @property float $exchanged_sale_total
 * @property float $exchanged_maker_total
 * @property float $exchanged_buy_fee
 * @property float $exchanged_sale_fee
 * @property float $exchanged_amount
 * @property float $ico_total_earned
 * @property float $ico_fee_earned
 * @property float $ico_total_sold
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read StockItem $baseItem
 * @property-read mixed $stock_pair
 * @property-read StockItem $stockItem
 * @method Builder|StockPair newModelQuery()
 * @method Builder|StockPair newQuery()
 * @method static Builder|StockPair query()
 * @mixin \Eloquent
 */
class StockPair extends Model
{

    protected $fillable = [
        'stock_item_id',
        'base_item_id',
        'is_active',
        'is_default',
        'base_item_buy_order_volume',
        'stock_item_buy_order_volume',
        'base_item_sale_order_volume',
        'stock_item_sale_order_volume',
        'exchanged_buy_total',
        'exchanged_sale_total',
        'exchanged_maker_total',
        'exchanged_amount',
        'exchanged_buy_fee',
        'exchanged_sale_fee',
        'last_price',
        'exchange_24',
        'ico_total_earned',
        'ico_fee_earned',
        'ico_total_sold',
    ];

    protected $fakeFields = [
        'stock_item_id',
        'base_item_id',
        'is_active',
        'is_default',
        'base_item_buy_order_volume',
        'stock_item_buy_order_volume',
        'base_item_sale_order_volume',
        'stock_item_sale_order_volume',
        'exchanged_buy_total',
        'exchanged_sale_total',
        'exchanged_maker_total',
        'exchanged_amount',
        'exchanged_buy_fee',
        'exchanged_sale_fee',
        'last_price',
        'exchange_24',
        'ico_total_earned',
        'ico_fee_earned',
        'ico_total_sold',
    ];

    public function stockItem(): BelongsTo
    {
        return $this->belongsTo(StockItem::class, 'stock_item_id', 'id');
    }

    public function baseItem(): BelongsTo
    {
        return $this->belongsTo(StockItem::class, 'base_item_id', 'id');
    }

    public function getStockPairAttribute(): string
    {
        return $this->stockItem->item . '/' . $this->baseItem->item;
    }
}
