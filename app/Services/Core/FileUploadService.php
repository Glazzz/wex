<?php

namespace App\Services\Core;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class FileUploadService
{
    public function upload(
        $file,
        string $filePath,
        string $fileName,
        string $prefix = '',
        string $suffix = '',
        ?string $disk = null,
        ?int $width = null,
        ?int $height = null,
        string $fileExtension = 'png',
        int $quality = 100
    ) {
        if (null === $disk) {
            $disk = config('filesystems.default');
        }

        $mimeType = $file->getClientMimeType();
        $imageMimeTypes = ['image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/svg+xml'];

        if (in_array($mimeType, $imageMimeTypes, true)) {
            $imageFile = Image::make($file);

            if (!is_null($width) && !is_null($height) && is_int($width) && is_int($height)) {
                $imageFile->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $background = Image::canvas($width, $height);
                $imageFile = $background->insert($imageFile, 'center');
            }

            $imageFile->encode($fileExtension, $quality);
            $fileName = md5($prefix . '_' . $fileName . '_' . $suffix) . '.' . $fileExtension;
            $path = $filePath . '/' . $fileName;
            $stored = Storage::disk($disk)->put($path, $imageFile->__toString());
        } else {
            $fileName = md5($prefix . '_' . $fileName . '_' . $suffix) . '.' . $file->getClientOriginalExtension();
            $stored = $file->storeAs($filePath, $fileName, $disk);
        }

        return isset($stored) ? $fileName : false;
    }
}
