<?php

namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

abstract class BaseRepository
{
    use RepositoryTrait;

    /** @var Model */
    protected $model;

    public function getAll($relations = null, $orders = null)
    {
        $query = $this->model->with($this->extractToArray($relations));

        if (!empty($orders) && \is_array($orders)) {
            $query = $query->orderBy($orders);
        }

        return $query->get();
    }

    public function getByConditions(array $conditions, $relations = null, $orders = null)
    {
        $query = $this->model->where($conditions)->with($this->extractToArray($relations));

        if (!empty($orders) && \is_array($orders)) {
            $query = $query->orderBy($orders);
        }

        return $query->get();
    }

    public function findOrFailById(int $id, $relations = null)
    {
        return $this->model->with($this->extractToArray($relations))->findOrFail($id);
    }

    public function findOrFailByConditions(array $conditions, $relations = null)
    {
        $instant = $this->model->where($conditions)->with($this->extractToArray($relations))->first();
        abort_if(empty($instant), 404);

        return $instant;
    }

    public function create(array $attributes)
    {
        return $this->model->create(array_only($attributes, $this->model->getFillable()));
    }

    public function update(array $attributes, int $id, string $attribute = 'id')
    {
        $instance = $this->getFirstByConditions([$attribute => $id]);

        if (empty($instance)) {
            return false;
        }

        return $instance->update(array_only($attributes, $this->model->getFillable())) ? $instance : false;
    }

    /**
     * @param string[]|string|null $relations
     * @param array $conditions
     */
    public function getFirstByConditions(array $conditions, $relations = null)
    {
        return $this->model->where($conditions)->with($this->extractToArray($relations))->first();
    }

    public function updateByConditions(array $attributes, array $conditions)
    {
        $instance = $this->getFirstByConditions($conditions);

        if (empty($instance)) {
            return false;
        }

        return $instance->update(array_only($attributes, $this->model->getFillable())) ? $instance : false;
    }

    public function deleteById(int $id)
    {
        $instance = $this->getFirstById($id);

        if (empty($instance)) {
            return false;
        }

        try {
            $instance->delete();
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    public function getFirstById(int $id, $relations = null)
    {
        return $this->model->where('id', $id)->with($this->extractToArray($relations))->first();
    }

    public function deleteByConditions(array $conditions)
    {
        $instance = $this->getFirstByConditions($conditions);

        if (empty($instance)) {
            return false;
        }

        try {
            $instance->delete();
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    public function toggleStatusById(int $id, string $attribute = 'is_active')
    {
        $instance = $this->getFirstById($id);

        if (empty($instance)) {
            return false;
        }

        $instance->{$attribute} = $instance->{$attribute} ? ACTIVE_STATUS_INACTIVE : ACTIVE_STATUS_ACTIVE;

        if ($instance->update()) {
            return $instance;
        }

        return $instance->update() ? $instance : false;
    }

    public function toggleStatusByConditions(array $conditions, string $attribute = 'is_active')
    {
        $instance = $this->getFirstByConditions($conditions);

        if (empty($instance)) {
            return false;
        }

        $instance->{$attribute} = $instance->{$attribute} ? ACTIVE_STATUS_INACTIVE : ACTIVE_STATUS_ACTIVE;

        return $instance->update() ? $instance : false;
    }

    public function insert(array $attributes)
    {
        return $this->model->insert($attributes);
    }

    public function bulkUpdate(array $values)
    {
        if (!\count($values)) {
            return false;
        }

        $table = $this->model->getTable();

        $sql = "UPDATE {$table} SET ";
        $updatableFieldSeparator = '';
        $rowCount = \count($values);
        $updatableFields = $this->_getUpdatableFields($values);
        if (!$updatableFields) {
            return false;
        }
        foreach ($updatableFields as $updatableField) {
            //for each updatable field
            $sql .= $updatableFieldSeparator;
            $sql .= "{$updatableField} = (CASE";
            for ($i = 0; $i < $rowCount; ++$i) {
                if (!isset($values[$i]['fields'][$updatableField])) {
                    continue;
                }
                $sql .= ' WHEN ';
                $conditionalSyntax = '';

                // for each condition
                foreach ($values[$i]['conditions'] as $conditionalKey => $conditionalValue) {
                    if (\is_array($conditionalValue)) {
                        $sql .= $conditionalSyntax."{$conditionalKey} {$conditionalValue[0]} {$conditionalValue[1]}";
                    } else {
                        $sql .= $conditionalSyntax."{$conditionalKey}='{$conditionalValue}'";
                    }
                    $conditionalSyntax = ' AND ';
                }

                $updatableFieldValue = $values[$i]['fields'][$updatableField];

                if (\is_array($updatableFieldValue)) {
                    if ('increment' === $updatableFieldValue[0]) {
                        $sql .= " THEN {$updatableField} + {$updatableFieldValue[1]}";
                    } elseif ('decrement' === $updatableFieldValue[0]) {
                        $sql .= " THEN {$updatableField} - {$updatableFieldValue[1]}";
                    } else {
                        $sql .= " THEN {$updatableFieldValue[1]}";
                    }
                } else {
                    $sql .= " THEN '{$updatableFieldValue}'";
                }
            }
            $sql .= " ELSE {$updatableField} END) ";
            $updatableFieldSeparator = ', ';
        }

        $conditionalClause = 'WHERE ';
        $conditionalFieldSeparator = '(';
        foreach ($values as $value) {
            $innerSeparator = '';
            $conditionalClause .= $conditionalFieldSeparator;
            foreach ($value['conditions'] as $conditionalKey => $conditionalValue) {
                if (\is_array($conditionalValue)) {
                    $conditionalClause .= $innerSeparator."{$conditionalKey} {$conditionalValue[0]} {$conditionalValue[1]}";
                } else {
                    $conditionalClause .= $innerSeparator."{$conditionalKey}='{$conditionalValue}'";
                }
                $innerSeparator = ' AND ';
            }
            $conditionalClause .= ')';
            $conditionalFieldSeparator = ' OR (';
        }
        $sql .= $conditionalClause;

        return DB::update($sql);
    }

    public function paginate(array $columns = ['*'], int $perPage = ITEM_PER_PAGE, string $paginationKey = 'p')
    {
        return $this->model->paginate($perPage, $columns, $paginationKey);
    }

    public function simplePaginate(array $columns = ['*'], int $perPage = ITEM_PER_PAGE, string $paginationKey = 'p', $where = null, $order = 'desc')
    {
        if (!empty($where)) {
            $this->model = $this->model->where($where);
        }
        if ('asc' !== $order) {
            $order = 'desc';
        }

        return $this->model->orderBy('id', $order)->simplePaginate($perPage, $columns, $paginationKey);
    }

    /**
     * @param (string|mixed)[][] $searchFields
     * @param (string|mixed)[][]|null $orderFields
     * @param array|null $whereArray
     * @param array|null $selectData
     * @param string[][]|null $joinArray
     * @param string[]|null $groupBy
     * @param mixed $paginationKey
     * @param mixed $dateField
     */
    public function filters(
        $searchFields,
        $orderFields = null,
        $whereArray = null,
        $selectData = null,
        $joinArray = null,
        $groupBy = null,
        $paginationKey = 'p',
        $dateField = 'created_at'
    ) {
        $tableName = $this->model->getTable();
        if (null !== $joinArray && \is_array($joinArray)) {
            $dateFieldChecker = explode('.', $dateField);
            if (1 == \count($dateFieldChecker)) {
                $dateField = $tableName.'.'.$dateField;
            }
        }
        $itemPerPage = admin_settings('item_per_page');
        $itemPerPage = filter_var($itemPerPage, FILTER_VALIDATE_INT) ? $itemPerPage : ITEM_PER_PAGE;
        $order = \Request::get($paginationKey.'_ord');
        $col = \Request::get($paginationKey.'_sort');
        $search = \Request::get($paginationKey.'_srch');
        $frm = \Request::get($paginationKey.'_frm');
        $to = \Request::get($paginationKey.'_to');
        $comp = \Request::get($paginationKey.'_comp');
        $ssf = \Request::get($paginationKey.'_ssf');

        if ('a' === $order) {
            $order = 'asc';
        } else {
            $order = 'desc';
        }

        $comparison = ['e' => '=', 'lk' => 'like', 'l' => '<', 'le' => '<=', 'm' => '>', 'me' => '>=', 'ne' => '!='];
        $comparison = \array_key_exists($comp, $comparison) ? $comparison[$comp] : $comparison = 'LIKE';
        if ($orderFields) {
            $allColumns = $orderFields;
        }

        $whereFields = array_column($searchFields, 0);
        $whereFields = \array_key_exists($ssf, $whereFields) ? $whereFields[$ssf] : array_values($whereFields);
        $elements = [
            $paginationKey.'_srch' => $search,
            $paginationKey.'_ord' => $order,
            $paginationKey.'_sort' => $col,
            $paginationKey.'_frm' => $frm,
            $paginationKey.'_to' => $to,
            $paginationKey.'_ssf' => $ssf,
            $paginationKey.'_comp' => $comp,
        ];
        if (isset($allColumns)) {
            $column = $this->_column_query($allColumns, $paginationKey.'_sort', $tableName.'.id');
        }

        $srcharr = 'like' === $comparison ? explode(' ', $search) : $search;

        foreach ($elements as $key => $val) {
            if (empty($val)) {
                unset($elements[$key]);
            } elseif ($key === $paginationKey.'_frm' || $key === $paginationKey.'_to') {
                if (!validate_date($val)) {
                    unset($elements[$key]);
                }
            }
        }

        if (null != $joinArray && \is_array($joinArray[0])) {
            foreach ($joinArray as $arr) {
                if (isset($arr[4])) {
                    $this->model = $this->model->leftJoin($arr[0], function ($join) use ($arr): void {
                        $join->on($arr[1], $arr[2], $arr[3])
                            ->where($arr[4]);
                    });
                } else {
                    $this->model = $this->model->leftJoin($arr[0], $arr[1], $arr[2], $arr[3]);
                }
            }
        } elseif (null != $joinArray) {
            if (isset($joinArray[4])) {
                $this->model = $this->model->leftJoin($joinArray[0], function ($join) use ($joinArray): void {
                    $join->on($joinArray[1], $joinArray[2], $joinArray[3])
                        ->where($joinArray[4]);
                });
            } else {
                $this->model = $this->model->leftJoin($joinArray[0], $joinArray[1], $joinArray[2], $joinArray[3]);
            }
        }

        if (isset($elements[$paginationKey.'_frm'])) {
            $this->model = $this->model->where($dateField, '>=', $frm);
        }
        if (isset($elements[$paginationKey.'_to'])) {
            $this->model = $this->model->where($dateField, '<', Carbon::parse($to)->addDay());
        }

        if (null !== $whereArray) {
            $this->model = $this->_where_builder($whereArray);
        }

        if (!empty($search)) {
            $this->model = $this->model->where(function ($query) use ($srcharr, $whereFields, $comparison): void {
                $firstQuery = 1;
                if (\is_array($srcharr) && 'like' === $comparison) {
                    foreach ($srcharr as $wh) {
                        if (\is_array($whereFields)) {
                            foreach ($whereFields as $sf) {
                                if (1 == $firstQuery) {
                                    $query->where($sf, 'like', '%'.$wh.'%');
                                } else {
                                    $query->orWhere($sf, 'like', '%'.$wh.'%');
                                }
                                $firstQuery = 0;
                            }
                        } else {
                            if (1 == $firstQuery) {
                                $query->where($whereFields, 'like', '%'.$wh.'%');
                            } else {
                                $query->orWhere($whereFields, 'like', '%'.$wh.'%');
                            }
                            $firstQuery = 0;
                        }
                    }
                } else {
                    if (\is_array($whereFields)) {
                        foreach ($whereFields as $sf) {
                            if (1 == $firstQuery) {
                                $query->where($sf, $comparison, $srcharr);
                            } else {
                                $query->orWhere($sf, $comparison, $srcharr);
                            }
                            $firstQuery = 0;
                        }
                    } else {
                        $query->where($whereFields, $comparison, $srcharr);
                    }
                }
            });
        }
        if (null != $selectData) {
            $this->model = $this->model->select($selectData);
        }
        if (!empty($groupBy)) {
            $this->model = $this->model->groupBy($groupBy);
        }

        if (!empty($column)) {
            $this->model = $this->model->orderBy($column, $order);
        }

        return $this->model->get();
    }

    /**
     * @param (string|mixed)[][] $searchFields
     * @param (string|mixed)[][]|null $orderFields
     * @param array|null $whereArray
     * @param (string|mixed)[]|null $selectData
     * @param ((class-string<\App\Models\User\Question>|class-string<\App\Models\Backend\Post>)[]|string)[][]|null $joinArray
     * @param string[]|null $groupBy
     * @param int|null $itemPerPage
     * @param mixed $paginationKey
     * @param mixed $dateField
     */
    public function paginateWithFilters(
        $searchFields,
        $orderFields = null,
        $whereArray = null,
        $selectData = null,
        $joinArray = null,
        $groupBy = null,
        $itemPerPage = null,
        $paginationKey = 'p',
        $dateField = 'created_at'
    ) {
        $tableName = $this->model->getTable();
        if (null !== $joinArray && \is_array($joinArray)) {
            $dateFieldChecker = explode('.', $dateField);
            if (1 == \count($dateFieldChecker)) {
                $dateField = $tableName.'.'.$dateField;
            }
        }
        $itemPerPage = empty($itemPerPage) ? admin_settings('item_per_page') : $itemPerPage;
        $itemPerPage = false != filter_var($itemPerPage, FILTER_VALIDATE_INT) ? $itemPerPage : ITEM_PER_PAGE;
        $order = \Request::get($paginationKey.'_ord');
        $col = \Request::get($paginationKey.'_sort');
        $search = \Request::get($paginationKey.'_srch');
        $frm = \Request::get($paginationKey.'_frm');
        $to = \Request::get($paginationKey.'_to');
        $comp = \Request::get($paginationKey.'_comp');
        $ssf = \Request::get($paginationKey.'_ssf');

        if ('a' === $order) {
            $order = 'asc';
        } else {
            $order = 'desc';
        }

        $comparison = ['e' => '=', 'lk' => 'like', 'l' => '<', 'le' => '<=', 'm' => '>', 'me' => '>=', 'ne' => '!='];
        $comparison = \array_key_exists($comp, $comparison) ? $comparison[$comp] : $comparison = 'LIKE';
        if ($orderFields) {
            $allcol = $orderFields;
        }

        $whereFields = array_column($searchFields, 0);
        $whereFields = \array_key_exists($ssf, $whereFields) ? $whereFields[$ssf] : array_values($whereFields);
        $elements = [$paginationKey.'_srch' => $search, $paginationKey.'_ord' => $order, $paginationKey.'_sort' => $col, $paginationKey.'_frm' => $frm, $paginationKey.'_to' => $to, $paginationKey.'_ssf' => $ssf, $paginationKey.'_comp' => $comp];
        if (isset($allcol)) {
            $column = $this->_column_query($allcol, $paginationKey.'_sort', $tableName.'.id');
        }

        $srcharr = 'like' === $comparison ? explode(' ', $search) : $search;

        foreach ($elements as $key => $val) {
            if (empty($val)) {
                unset($elements[$key]);
            } elseif ($key === $paginationKey.'_frm' || $key === $paginationKey.'_to') {
                if (!validate_date($val)) {
                    unset($elements[$key]);
                }
            }
        }

        if (null != $joinArray && \is_array($joinArray[0])) {
            foreach ($joinArray as $arr) {
                if (isset($arr[4])) {
                    $this->model = $this->model->leftJoin($arr[0], function ($join) use ($arr): void {
                        $join->on($arr[1], $arr[2], $arr[3])
                            ->where($arr[4]);
                    });
                } else {
                    $this->model = $this->model->leftJoin($arr[0], $arr[1], $arr[2], $arr[3]);
                }
            }
        } elseif (null != $joinArray) {
            if (isset($joinArray[4])) {
                $this->model = $this->model->leftJoin($joinArray[0], function ($join) use ($joinArray): void {
                    $join->on($joinArray[1], $joinArray[2], $joinArray[3])
                        ->where($joinArray[4]);
                });
            } else {
                $this->model = $this->model->leftJoin($joinArray[0], $joinArray[1], $joinArray[2], $joinArray[3]);
            }
        }

        if (isset($elements[$paginationKey.'_frm'])) {
            $this->model = $this->model->where($dateField, '>=', $frm);
        }
        if (isset($elements[$paginationKey.'_to'])) {
            $this->model = $this->model->where($dateField, '<', Carbon::parse($to)->addDay());
        }

        if (null !== $whereArray) {
            $this->model = $this->_where_builder($whereArray);
        }

        if (!empty($search)) {
            $this->model = $this->model->where(function ($query) use ($srcharr, $whereFields, $comparison): void {
                $firstQuery = 1;
                if (\is_array($srcharr) && 'like' === $comparison) {
                    foreach ($srcharr as $wh) {
                        if (\is_array($whereFields)) {
                            foreach ($whereFields as $sf) {
                                if (1 == $firstQuery) {
                                    $query->where($sf, 'like', '%'.$wh.'%');
                                } else {
                                    $query->orWhere($sf, 'like', '%'.$wh.'%');
                                }
                                $firstQuery = 0;
                            }
                        } else {
                            if (1 == $firstQuery) {
                                $query->where($whereFields, 'like', '%'.$wh.'%');
                            } else {
                                $query->orWhere($whereFields, 'like', '%'.$wh.'%');
                            }
                            $firstQuery = 0;
                        }
                    }
                } else {
                    if (\is_array($whereFields)) {
                        foreach ($whereFields as $sf) {
                            if (1 == $firstQuery) {
                                $query->where($sf, $comparison, $srcharr);
                            } else {
                                $query->orWhere($sf, $comparison, $srcharr);
                            }
                            $firstQuery = 0;
                        }
                    } else {
                        $query->where($whereFields, $comparison, $srcharr);
                    }
                }
            });
        }
        if (null != $selectData) {
            $this->model = $this->model->select($selectData);
        }
        if (!empty($groupBy)) {
            $this->model = $this->model->groupBy($groupBy);
        }

        if (!empty($column)) {
            $this->model = $this->model->orderBy($column, $order);
        } else {
            $this->model = $this->model->orderBy($tableName.'.id', $order);
        }

        return $this->model->paginate($itemPerPage, ['*'], $paginationKey)->appends($elements);
    }
}
