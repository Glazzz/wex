<?php

namespace App\Http\Middleware;

use Closure;

class ExchangePair
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Null or item-item
        if (null === $request->pair || \preg_match('/([a-zA-Z0-9]){2,5}-([a-zA-Z0-9]){2,5}/', $request->pair)) {
            return $next($request);
        }

        abort(404);
    }
}
