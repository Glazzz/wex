<?php

namespace App\Http\Controllers;

use App\Jobs\StopLimitStockOrder;
use Illuminate\Http\JsonResponse;

class TestController extends Controller
{
    public function test(): void
    {
        $this->dispatch(new StopLimitStockOrder(1,1.20000000));
    }

    /**
     * Check status of project. Need for docker containers
     *
     * @return JsonResponse
     */
    public function healthy(): JsonResponse
    {
        return response()->json([
            'status' => true,
        ]);
    }
}
