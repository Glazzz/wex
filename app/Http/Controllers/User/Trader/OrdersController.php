<?php

namespace App\Http\Controllers\User\Trader;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\Trader\StockOrderRequest;
use App\Repositories\User\Trader\Eloquent\StockOrderRepository;
use App\Repositories\User\Trader\Interfaces\StockOrderInterface;
use App\Services\User\Trader\StockOrderService;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class OrdersController extends Controller
{
    private $stockOrderService;

    /**
     * @param StockOrderService $stockOrderService
     */
    public function __construct(StockOrderService $stockOrderService)
    {
        $this->stockOrderService = $stockOrderService;
    }

    public function openOrders()
    {
        $data['list'] = $this->stockOrderService->openOrders();
        $data['title'] = __('Open Orders');

        return view('frontend.orders.open_orders', $data);
    }

    /**
     * @param StockOrderRequest $request
     *
     * @return Response
     */
    public function store(StockOrderRequest $request): Response
    {
        $response = app(StockOrderService::class)->order($request);

        if ($response[SERVICE_RESPONSE_STATUS]) {
            return response()->json([SERVICE_RESPONSE_SUCCESS => $response[SERVICE_RESPONSE_MESSAGE]]);
        }

        return response()->json([SERVICE_RESPONSE_ERROR => $response[SERVICE_RESPONSE_MESSAGE]]);

    }

    /**
     * @param StockOrderRepository|StockOrderInterface $stockOrderRepository
     * @param int $id
     *
     * @return Response
     */
    public function destroy(StockOrderInterface $stockOrderRepository, $id): Response
    {
        $stockOrder = $stockOrderRepository->getFirstById($id);

        if (empty($stockOrder)) {
            return response()->json([
                SERVICE_RESPONSE_ERROR => __('Order not found.'),
            ]);
        }


        if (Auth::id() != $stockOrder->user_id) {
            return response()->json([
                SERVICE_RESPONSE_ERROR => __('You are not authorize to do this action.'),
            ]);
        }

        if ($stockOrder->status >= STOCK_ORDER_COMPLETED) {
            return response()->json([
                SERVICE_RESPONSE_ERROR => __('This order cannot be deleted.'),
            ]);
        }


        $response = app(StockOrderService::class)->cancelOrder($id);

        if ($response[SERVICE_RESPONSE_STATUS]) {
            return response()->json([
                SERVICE_RESPONSE_SUCCESS => $response[SERVICE_RESPONSE_MESSAGE],
            ]);
        }

        return response()->json([
            SERVICE_RESPONSE_ERROR => $response[SERVICE_RESPONSE_MESSAGE],
        ]);
    }
}