<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\Api\BitcoinApi;
use App\Services\Api\CoinPaymentApi;
use App\Services\User\Trader\WalletService;
use Illuminate\Http\Request;

class IpnController extends Controller
{
    public function ipn(Request $request)
    {
        $ipnRequest = $request->all();

        if('production' !== env('APP_ENV') && env('APP_DEBUG')) {
            logs()->info($ipnRequest);
        }

        if(empty($ipnRequest) || !isset($ipnRequest['currency'])) {
            logs()->error('log: Invalid coinpayment IPN request.');

            return null;
        }

        $coinPaymentApi = new CoinPaymentApi($ipnRequest['currency']);
        $ipnResponse = $coinPaymentApi->validateIPN($ipnRequest, $request->server());

        if( 'ok' === $ipnResponse['error']) {
            app(WalletService::class)->updateTransaction($ipnResponse);

            return null;
        }

        logs()->error($ipnResponse['error']);

        return null;
    }

    public function bitcoinIpn(Request $request, $currency)
    {
        try {
            $bitcoin = new BitcoinApi($currency);
            $ipnResponse = $bitcoin->validateIPN($request->all(), $request->server());

            if('ok' === $ipnResponse['error']) {
                app(WalletService::class)->updateTransaction($ipnResponse);
            } else{
                logs()->error($ipnResponse['error']);

                return null;
            }
        }
        catch (\Exception $exception)
        {
            logs()->error( $exception->getMessage() );

            return null;
        }
    }
}
